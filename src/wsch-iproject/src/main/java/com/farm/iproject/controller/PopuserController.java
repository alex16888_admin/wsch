package com.farm.iproject.controller;

import com.farm.iproject.domain.PopUser;
import com.farm.iproject.service.PopuserServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.query.DataQuerys;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：权限用户控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/popuser")
@Controller
public class PopuserController extends WebUtils {
	private final static Logger log = Logger.getLogger(PopuserController.class);
	@Resource
	private PopuserServiceInter popUserServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String groupids, DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataQuerys.wipeVirusNoDot(groupids);
			query.addSqlRule(" and groupid in (" + DataQuerys.getWhereInSubVals(parseIds(groupids)) + ")");
			DataResult result = popUserServiceImpl.createPopuserSimpleQuery(query).search();
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					if (row.get("USERID").equals("ANONYMOUS")) {
						row.put("USERNAME", "【匿名用户】");
					}
				}
			});
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(PopUser entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = popUserServiceImpl.editPopuserEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(PopUser entity, HttpSession session) {
		try {
			for (String groupid : parseIds(entity.getGroupid())) {
				// entity.setGroupid(groupid);
				// entity = popUserServiceImpl.insertPopuserEntity(entity,
				// getCurrentUser(session));
				popUserServiceImpl.addGroupUser(groupid, entity.getUserid());
			}
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				popUserServiceImpl.deletePopuserEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String groupids, HttpSession session) {
		return ViewMode.getInstance().putAttr("groupids", groupids).returnModelAndView("iproject/PopuserResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(String groupids, RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", popUserServiceImpl.getPopuserEntity(ids))
						.returnModelAndView("iproject/PopuserForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("groupids", groupids).putAttr("pageset", pageset)
						.returnModelAndView("iproject/PopuserForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", popUserServiceImpl.getPopuserEntity(ids))
						.returnModelAndView("iproject/PopuserForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/PopuserForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/PopuserForm");
		}
	}
}
