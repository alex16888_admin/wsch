package com.farm.iproject.service;

import com.farm.iproject.domain.Project;
import com.farm.wsch.index.WschIndexServerInter;
import com.farm.wsch.index.WschSearchServerInter;
import com.farm.wsch.index.pojo.IndexConfig;
import com.farm.core.sql.query.DataQuery;

import java.io.IOException;
import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：检索项目服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Project insertProjectEntity(Project entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Project editProjectEntity(Project entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjectEntity(String id, LoginUser user);

	/**
	 * 判断是否有重复key（相同id的不算重复）
	 * 
	 * @param keyid
	 * @param id
	 * @return
	 */
	public boolean isKeyidRepeat(String keyid, String id);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Project getProjectEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjectSimpleQuery(DataQuery query);

	/**
	 * 获得项目的索引文件夹
	 * 
	 * @param projectid
	 * @return
	 */
	public String getIndexPath(String projectid);

	/**
	 * 獲得項目的附件文件夾
	 * 
	 * @param projectid
	 * @return
	 */
	public String getFilesPath(String projectid);

	/**
	 * 获得索引服务
	 * 
	 * @param projectid
	 * @return
	 * @throws IOException
	 */
	public WschIndexServerInter getWschIndexServer(String projectid) throws IOException;

	/**
	 * 獲得檢索服務
	 * 
	 * @param projectid
	 * @return
	 */
	public WschSearchServerInter getWschSearchServer(String projectid) throws IOException;

	/**
	 * 获得索引配置
	 * 
	 * @param projectid
	 * @return
	 */
	public IndexConfig getIndexConfig(String projectid);

	/**
	 * 获得所有项目
	 * 
	 * @return
	 */
	public List<Project> getProjects();

	/**
	 * 合并项目索引
	 * 
	 * @param projectid
	 * @param currentUser
	 */
	public void mergeIndex(String projectid, LoginUser currentUser);

	/**
	 * 获得项目
	 * 
	 * @param key
	 * @return
	 */
	public Project getProjectByKey(String key);
}