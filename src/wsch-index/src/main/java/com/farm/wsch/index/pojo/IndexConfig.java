package com.farm.wsch.index.pojo;

import java.io.File;

import com.farm.parameter.FarmParameterService;

/**
 * 索引对象参数封装
 * 
 * @author macpl
 *
 */
public class IndexConfig {
	/**
	 * 索引目录
	 */
	private File indexDir;
	/**
	 * 高亮标签
	 */
	private String highLightTagStart = "<span class='high-light'>";
	/**
	 * 高亮标签
	 */
	private String highLightTagEnd = "</span>";

	private int searchTopNum = 2000;

	/**
	 * 高亮段落长度/检索结果中用于缩略描述展示的字符长度
	 */
	private int highLightLength = 100;

	public IndexConfig(String indexDirPath) {
		searchTopNum = FarmParameterService.getInstance().getParameterInt("config.index.search.top.num");
		highLightLength = FarmParameterService.getInstance().getParameterInt("config.index.search.content.size");
		highLightTagStart = FarmParameterService.getInstance().getParameter("config.index.search.highlt.start");
		highLightTagEnd = FarmParameterService.getInstance().getParameter("config.index.search.highlt.end");
		setIndexDir(indexDirPath);
	}

	public File getIndexDir() {
		return indexDir;
	}

	public void setIndexDir(File indexDir) {
		this.indexDir = indexDir;
	}

	public void setIndexDir(String indexDirPath) {
		this.indexDir = new File(indexDirPath);
	}

	public String getHighLightTagStart() {
		return highLightTagStart;
	}

	public void setHighLightTagStart(String highLightTagStart) {
		this.highLightTagStart = highLightTagStart;
	}

	public String getHighLightTagEnd() {
		return highLightTagEnd;
	}

	public void setHighLightTagEnd(String highLightTagEnd) {
		this.highLightTagEnd = highLightTagEnd;
	}

	public int getHighLightLength() {
		return highLightLength;
	}

	public void setHighLightLength(int highLightLength) {
		this.highLightLength = highLightLength;
	}

	public int getSearchTopNum() {
		return searchTopNum;
	}

}
