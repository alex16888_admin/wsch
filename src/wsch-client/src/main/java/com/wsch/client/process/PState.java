package com.wsch.client.process;

/**
 * 运行状态
 * 
 * @author macpl
 *
 */
public class PState {
	/**
	 * 最大进度
	 */
	private int mnum = 0;
	/**
	 * 当前进度
	 */
	private int cnum = 0;
	private String message;
	private StateEnum state = StateEnum.ready;

	public enum StateEnum {
		// 准备，执行中，结束，错误
		ready, running, finish, error
	}

	public StateEnum getState() {
		return state;
	}

	public void setState(StateEnum state) {
		this.state = state;
	}

	public int getMnum() {
		return mnum;
	}

	public void setMnum(int mnum) {
		this.mnum = mnum;
	}

	public int getCnum() {
		return cnum;
	}

	public void setCnum(int cnum) {
		this.cnum = cnum;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	};

}
