package com.farm.wsch.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.farm.wsch.index.pojo.IndexDocument;

public interface WschIndexServerInter {
	/**
	 * 默认域
	 * 
	 * @author macpl
	 *
	 */
	public enum DefaultField {
		APPID("文档ID"), APPKEYID("业务KEYID"), POPKEYS("权限KEY");

		String title;
		String key;

		DefaultField(String title) {
			this.title = title;
			this.key = this.name();
		}

		public static List<DefaultField> getList() {
			List<DefaultField> list = new ArrayList<DefaultField>();
			for (DefaultField field : values()) {
				list.add(field);
			}
			return list;
		}

		public static Map<String, String> getDic() {
			Map<String, String> map = new HashMap<String, String>();
			for (DefaultField field : values()) {
				map.put(field.getKey(), field.getTitle());
			}
			return map;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}

	/**
	 * 域类型
	 * 
	 * @author macpl
	 *
	 */
	public enum FieldType {
		TextField("文本"), StringField("单词"), FloatField("数值");

		String title;
		String key;

		FieldType(String title) {
			this.title = title;
			this.key = this.name();
		}

		public static List<FieldType> getList() {
			List<FieldType> list = new ArrayList<FieldType>();
			for (FieldType field : values()) {
				list.add(field);
			}
			return list;
		}

		public static Map<String, String> getDic() {
			Map<String, String> map = new HashMap<String, String>();
			for (FieldType field : values()) {
				map.put(field.getKey(), field.getTitle());
			}
			return map;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public static FieldType getType(String type) {
			for (FieldType field : values()) {
				if (type.equals(field.name())) {
					return field;
				}
			}
			throw new RuntimeException("the type(" + type + ") is not exist!");
		}

	}

	/**
	 * 提交一个文档
	 * 
	 * @throws IOException
	 */
	public void addDocument(IndexDocument idoc) throws IOException;

	/**
	 * 提交索引
	 * 
	 * @throws IOException
	 */
	public void closeAndSubmit() throws IOException;

	/**
	 * 更新一個文檔
	 * 
	 * @param idoc
	 * @throws IOException
	 */
	public void updateDocument(IndexDocument idoc) throws IOException;

	/**
	 * 合并索引
	 * 
	 * @param dir file
	 * @throws IOException
	 */
	public void mergeIndex() throws IOException;

	/**
	 * 删除一个文档
	 * 
	 * @param docid
	 * @throws IOException
	 */
	public void delDocument(String docid) throws IOException;

	/**
	 * 删除所有索引
	 */
	public void delAllIndex() throws IOException;

	/**
	 * 删除一业务类型的文档
	 * 
	 * @param appkeyid
	 * @throws IOException
	 */
	public void delAppDocuments(String appkeyid) throws IOException;
}
