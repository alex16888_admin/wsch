package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Appmodel;
import org.apache.log4j.Logger;

import com.farm.iproject.dao.AppdicDaoInter;
import com.farm.iproject.dao.AppmodelDaoInter;
import com.farm.iproject.dao.VirtualfieldDaoInter;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务类型服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class AppmodelServiceImpl implements AppmodelServiceInter {
	@Resource
	private AppmodelDaoInter appmodelDaoImpl;
	@Resource
	private VirtualfieldDaoInter virtualfieldDaoImpl;
	@Resource
	private AppdicDaoInter appdicDaoImpl;
	private static final Logger log = Logger.getLogger(AppmodelServiceImpl.class);

	@Override
	@Transactional
	public Appmodel insertAppmodelEntity(Appmodel entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		// entity.setSort(1);
		return appmodelDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Appmodel editAppmodelEntity(Appmodel entity, LoginUser user) {
		Appmodel entity2 = appmodelDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		// entity2.setProjectid(entity.getProjectid());
		entity2.setSort(entity.getSort());
		entity2.setKeyid(entity.getKeyid());
		entity2.setName(entity.getName());
		// entity2.setId(entity.getId());
		appmodelDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteAppmodelEntity(String id, LoginUser user) {
		virtualfieldDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("APPMODELID", id, "=")).toList());
		appdicDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("APPID", id, "=")).toList());
		appmodelDaoImpl.deleteEntity(appmodelDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Appmodel getAppmodelEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return appmodelDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createAppmodelSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WSCH_P_APPMODEL", "ID,PROJECTID,SORT,KEYID,NAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public Object getAppModelNum(String projectid) {
		DBRuleList rules = DBRuleList.getInstance();
		rules.add(new DBRule("PROJECTID", projectid, "="));
		return appmodelDaoImpl.countEntitys(rules.toList());
	}

	@Override
	@Transactional
	public List<Appmodel> getAppmodels(String projectid) {
		List<Appmodel> appmodels = appmodelDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "=")).toList());
		Collections.sort(appmodels, new Comparator<Appmodel>() {
			@Override
			public int compare(Appmodel o1, Appmodel o2) {
				return o1.getSort() - o2.getSort();
			}
		});
		return appmodels;
	}

	@Override
	@Transactional
	public Appmodel getAppmodel(String projectid, String key) {
		List<Appmodel> appmodels = appmodelDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "=")).toList());
		for (Appmodel node : appmodels) {
			if (node.getKeyid().trim().equals(key.trim())) {
				return node;
			}
		}
		return null;
	}

	@Override
	@Transactional
	public Map<String, Appmodel> getAppmodelMap(String projectid) {
		Map<String, Appmodel> map = new HashMap<String, Appmodel>();
		List<Appmodel> list = getAppmodels(projectid);
		for (Appmodel node : list) {
			map.put(node.getKeyid(), node);
		}
		return map;
	}

}
