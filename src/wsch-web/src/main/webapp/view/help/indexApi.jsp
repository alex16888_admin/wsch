<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>indexAPI-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<script type="text/javascript" src="text/javascript/jquery1113.js"></script>
<link href="text/lib/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
<link href="text/lib/bootstrap3/css/bootstrap-theme.min.css"
	rel="stylesheet">
<script src="text/lib/bootstrap3/js/bootstrap.min.js"></script>
<script src="text/lib/bootstrap3/respond.min.js"></script>
<script type="text/javascript">
	var basePath = '<PF:basePath/>';
	$(function() {
		$.ajaxSetup({
			cache : false
		});
	})
//-->
</script>
<style>
<!--
.container h1 {
	font-weight: 700;
	margin: 10px;
}

.container h3 {
	margin: 10px;
	color: #CE4844;
}

.container .protocol {
	color: #4fadc2;
	font-size: 24px;
	font-weight: 500px;
}

.container .demo {
	color: #4fadc2;
	max-width: 400px;
}

ul li {
	font-size: 18px;
	font-weight: 200;
}

h1 a {
	font-size: 12px;
	margin-left: 24px;
	padding-left: 24px;
	font-weight: 200;
}

.apimenu {
	
}

.apimenu li {
	margin: 0px;
	padding: 2px;
	line-height: 14px;
	font-size: 14px;
}

.apimenu li a {
	margin: 0px;
	padding: 4px;
}

.page-header {
	padding-top: 50px;
}

caption {
	text-align: left;
	color: #CE4844;
}
-->
</style>
</head>
<body class="container" style="padding: 16px;">
	<div class="row">
		<div class="col-sm-3 ">
			<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix">
				<ul class="nav bs-docs-sidenav apimenu">
					<li><a href="helper/index.do#projectGetApi">项目----查询----[<b>项目状态</b>]
					</a></li>
					<li><a href="helper/index.do#registFieldApi">项目----注册----1.[<b>索引域</b>]
					</a></li>
					<li><a href="helper/index.do#registAppModelApi">项目----注册----2.[<b>检索业务</b>]
					</a></li>
					<li><a href="helper/index.do#registVirtualFieldApi">项目----注册----3.[<b>业务虚拟域</b>]
					</a></li>
					<li><hr style="margin: 1px;" /></li>
					<li><a href="helper/index.do#appDicRegist">检索字典----注册----1.[<b>检索字典</b>]
					</a></li>
					<li><a href="helper/index.do#appDicNodeClear">检索字典----清空子项----2.[<b>检索字典</b>]
					</a></li>
					<li><a href="helper/index.do#appDicNodeAdd">检索字典----添加子项----3.[<b>检索字典</b>]
					</a></li>
					<li><hr style="margin: 1px;" /></li>
					<li><a href="helper/index.do#PopGroupDel">检索权限----删除权限组 </a></li>
					<li><a href="helper/index.do#PopGroupAdd">检索权限----初始化权限组----1.[<b>检索权限</b>]
					</a></li>
					<li><a href="helper/index.do#PopGroupAddUser">检索权限----添加权限组-用户----2.[<b>检索权限</b>]
					</a></li>
					<li><a href="helper/index.do#PopGroupAddKey">检索权限----添加权限组-权限----3.[<b>检索权限</b>]
					</a></li>
					<li><hr style="margin: 1px;" /></li>
					<li><a href="helper/index.do#indexAddApi">索引----[<b>预载索引</b>]
					</a></li>
					<li><a href="helper/index.do#indexRunApi">索引----[<b>执行索引</b>]
					</a></li>
					<li><a href="helper/index.do#indexDelApi">索引----[<b>删除索引</b>]
					</a></li>
					<li><a href="helper/index.do#indexClearApi">索引----[<b>删除全部索引</b>]
					</a></li>
					<li><hr style="margin: 1px;" /></li>
					<li><a href="helper/readme.do">返回 </a></li>
				</ul>
			</nav>
		</div>
		<div class="col-sm-9">

			<!-- 项目信息查询-->
			<div id="projectGetApi" class="page-header">
				<jsp:include page="indexComment/projectGetApi.jsp"></jsp:include>
			</div>

			<!-- 注册1索引域-->
			<div id="registFieldApi" class="page-header">
				<jsp:include page="indexComment/registFieldApi.jsp"></jsp:include>
			</div>
			<!-- 注册2检索业务-->
			<div id="registAppModelApi" class="page-header">
				<jsp:include page="indexComment/registAppModelApi.jsp"></jsp:include>
			</div>
			<!-- 注册3业务虚拟域-->
			<div id="registVirtualFieldApi" class="page-header">
				<jsp:include page="indexComment/registVirtualFieldApi.jsp"></jsp:include>
			</div>
			<!-- 检索字典----注册-->
			<div id="appDicRegist" class="page-header">
				<jsp:include page="indexComment/appDicRegist.jsp"></jsp:include>
			</div>
			<!-- 检索字典----清空子项-->
			<div id="appDicNodeClear" class="page-header">
				<jsp:include page="indexComment/appDicNodeClear.jsp"></jsp:include>
			</div>
			<!-- 检索字典----添加子项-->
			<div id="appDicNodeAdd" class="page-header">
				<jsp:include page="indexComment/appDicNodeAdd.jsp"></jsp:include>
			</div>
			<!-- PopGroupDel 检索权限----删除权限组-->
			<div id="PopGroupDel" class="page-header">
				<jsp:include page="indexComment/PopGroupDel.jsp"></jsp:include>
			</div>
			<!-- PopGroupAdd 检索权限----初始化权限组-->
			<div id="PopGroupAdd" class="page-header">
				<jsp:include page="indexComment/PopGroupAdd.jsp"></jsp:include>
			</div>
			<!-- PopGroupAddUser 检索权限----添加权限组-用户-->
			<div id="PopGroupAddUser" class="page-header">
				<jsp:include page="indexComment/PopGroupAddUser.jsp"></jsp:include>
			</div>
			<!-- PopGroupAddKey 检索权限----添加权限组-权限-->
			<div id="PopGroupAddKey" class="page-header">
				<jsp:include page="indexComment/PopGroupAddKey.jsp"></jsp:include>
			</div>
			<!-- 预载索引-->
			<div id="indexAddApi" class="page-header">
				<jsp:include page="indexComment/indexAddApi.jsp"></jsp:include>
			</div>
			<!-- 执行索引-->
			<div id="indexRunApi" class="page-header">
				<jsp:include page="indexComment/indexRunApi.jsp"></jsp:include>
			</div>
			<!-- 删除索引-->
			<div id="indexDelApi" class="page-header">
				<jsp:include page="indexComment/indexDelApi.jsp"></jsp:include>
			</div>
			<!-- 全部删除索引-->
			<div id="indexClearApi" class="page-header">
				<jsp:include page="indexComment/indexClearApi.jsp"></jsp:include>
			</div>
		</div>
	</div>
</body>
</html>