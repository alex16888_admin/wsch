package com.farm.iproject.controller;

import com.farm.iproject.domain.Appmodel;
import com.farm.iproject.domain.Project;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.iproject.service.ProjectServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：业务类型控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/appmodel")
@Controller
public class AppmodelController extends WebUtils {
	private final static Logger log = Logger.getLogger(AppmodelController.class);
	@Resource
	private AppmodelServiceInter appModelServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String projectid, DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("projectid", projectid, "="));query.addDefaultSort(new DBSort("SORT","ASC"));
			query.setPagesize(100);
			DataResult result = appModelServiceImpl.createAppmodelSimpleQuery(query).search();
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Appmodel entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = appModelServiceImpl.editAppmodelEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Appmodel entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = appModelServiceImpl.insertAppmodelEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				appModelServiceImpl.deleteAppmodelEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String projectid, HttpSession session) {
		return ViewMode.getInstance().putAttr("projectid", projectid).returnModelAndView("iproject/AppmodelResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(String projectid, RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				Appmodel appmodel = appModelServiceImpl.getAppmodelEntity(ids);
				Project project = projectServiceImpl.getProjectEntity(appmodel.getProjectid());
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.putAttr("entity", appmodel).returnModelAndView("iproject/AppmodelForm");
			}
			case (1): {// 新增
				Project project = projectServiceImpl.getProjectEntity(projectid);
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.returnModelAndView("iproject/AppmodelForm");
			}
			case (2): {// 修改
				Appmodel appmodel = appModelServiceImpl.getAppmodelEntity(ids);
				Project project = projectServiceImpl.getProjectEntity(appmodel.getProjectid());
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.putAttr("entity", appmodel).returnModelAndView("iproject/AppmodelForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/AppmodelForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/AppmodelForm");
		}
	}
}
