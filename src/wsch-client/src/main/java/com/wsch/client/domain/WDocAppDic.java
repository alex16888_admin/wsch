package com.wsch.client.domain;

import java.util.HashMap;
import java.util.Map;

public class WDocAppDic {

	public enum LIKETYPES {
		// 1.完全匹配，2字典为字段子集
		all("1"), indexOf("2");

		private String val;

		private LIKETYPES(String val) {
			this.val = val;
		}

		public String getVal() {
			return val;
		}

	}

	public enum ABLETYPES {
		// 1.完全匹配，2字典为字段子集
		result("1"), all("2");

		private String val;

		private ABLETYPES(String val) {
			this.val = val;
		}

		public String getVal() {
			return val;
		}

	}

	public enum SUMABLES {
		// 1.完全匹配，2字典为字段子集
		able("1"), unable("2");

		private String val;

		private SUMABLES(String val) {
			this.val = val;
		}

		public String getVal() {
			return val;
		}

	}

	/**
	 * @param keyid 一级字典的key
	 * @param fieldkey
	 * @param liketype
	 * @param abletype
	 * @param sumable
	 * @param name
	 * @param sort
	 * @param appkeys  检索业务key(多个间逗号分隔)
	 */
	public WDocAppDic(String keyid, String fieldkey, LIKETYPES liketype, ABLETYPES abletype, SUMABLES sumable,
			String name, int sort, String appkeys) {
		super();
		this.fieldkey = fieldkey;
		this.liketype = liketype;
		this.abletype = abletype;
		this.keyid = keyid;
		this.sumable = sumable;
		this.name = name;
		this.sort = sort;
		this.appkeys = appkeys;
	}

	private String fieldkey;
	private LIKETYPES liketype;
	private ABLETYPES abletype;
	private SUMABLES sumable;
	private String name;
	private int sort;
	private String appkeys;
	private String keyid;

	public Map<String, String> getMap() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fieldkey", fieldkey);
		data.put("liketype", liketype.getVal());
		data.put("abletype", abletype.getVal());
		data.put("sumable", sumable.getVal());
		data.put("keyid", keyid);
		data.put("name", name);
		data.put("sort", new Integer(sort).toString());
		data.put("appkeys", appkeys);
		return data;
	}

	public String getKeyid() {
		return keyid;
	}

	public void setKeyid(String keyid) {
		this.keyid = keyid;
	}

	public String getAppkeys() {
		return appkeys;
	}

	public String getFieldkey() {
		return fieldkey;
	}

	public LIKETYPES getLiketype() {
		return liketype;
	}

	public ABLETYPES getAbletype() {
		return abletype;
	}

	public SUMABLES getSumable() {
		return sumable;
	}

	public String getName() {
		return name;
	}

	public int getSort() {
		return sort;
	}

}
