<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false">
		<table id="dataAppdicGrid">
			<thead>
				<tr>
					<th field="DICNAME" data-options="sortable:true" width="50">字典</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionAppdic = "appdic/del.do";//删除URL
	var url_formActionAppdic = "appdic/form.do";//增加、修改、查看URL
	var url_searchActionAppdic = "appdic/query.do";//查询URL
	var title_windowAppdic = "业务字典管理";//功能名称
	var gridAppdic;//数据表格对象
	var searchAppdic;//条件查询组件对象
	var toolBarAppdic = [ {
		id : 'add',
		text : '添加',
		iconCls : 'icon-add',
		handler : addDataAppdic
	}, {
		id : 'del',
		text : '移除',
		iconCls : 'icon-remove',
		handler : delDataAppdic
	} ];
	$(function() {
		//初始化数据表格
		gridAppdic = $('#dataAppdicGrid').datagrid({
			url : url_searchActionAppdic,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarAppdic,
			pagination : false,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchAppdic = $('#searchAppdicForm').searchForm({
			gridObj : gridAppdic
		});
	});
	//查看
	function viewDataAppdic() {
		var selectedArray = $(gridAppdic).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionAppdic + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winAppdic',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataAppdic() {
		var selectedApp = $(gridAppmodel).datagrid('getSelections');
		if (!selectedApp[0]) {
			alert('请选择左侧业务!');
		}
		var appid = selectedApp[0].ID
		var url = url_formActionAppdic + '?appmodelid=' + appid
				+ '&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winAppdic',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataAppdic() {
		var selectedArray = $(gridAppdic).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionAppdic + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winAppdic',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataAppdic() {
		var selectedArray = $(gridAppdic).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridAppdic).datagrid('loading');
					$.post(url_delActionAppdic + '?ids='
							+ $.farm.getCheckedIds(gridAppdic, ''), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridAppdic).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridAppdic).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>