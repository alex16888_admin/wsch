<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>图片预览-<PF:ParameterValue key="config.sys.title" /></title>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
<script type="text/javascript" src="text/javascript/jquery1113.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/viewer/viewer.js"></script>
<link href="<PF:basePath/>text/lib/viewer/viewer.css" rel="stylesheet">
<link href="text/lib/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
<link href="text/lib/bootstrap3/css/bootstrap-theme.min.css"
	rel="stylesheet">
<script src="text/lib/bootstrap3/js/bootstrap.min.js"></script>
</head>
<body oncontextmenu="customContextMenu(event)"
	style="padding: 0px; margin: 0px; background-color: #000000;">
	<div
		style="background-color: #000000; border-radius: 15px 15px 15px 15px; padding: 10px; text-align: center;">
		<!-- 左侧导航 -->
		<div class="hidden-xs"
			style="float: left; width: 100px; overflow: auto;" id="imgsMinBoxDiv">
			<c:forEach begin="1" end="${pinum}" var="page">
				<c:if test="${page<10}">
					<img id="Page1" alt="${page}" class="img-responsive pageName${page}"
						onclick="scrollToMarkName('.pageName${page}')"
						src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						data-src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						style="margin: auto; width: 64px; cursor: pointer;"
						src="text/img/demo/playerIcon.png">
					<div
						style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 10px;">第${page}页/共${pinum}页
					</div>
				</c:if>
				<c:if test="${page>=10}">
					<img id="Page1" alt="${page}" class="img-responsive pageName${page}"
						onclick="scrollToMarkName('.pageName${page}')"
						data-src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						style="margin: auto; width: 64px; cursor: pointer;"
						src="text/img/demo/playerIcon.png">
					<div
						style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 10px;">第${page}页/共${pinum}页
					</div>
				</c:if>
			</c:forEach>
		</div>
		<!-- 图片区域 -->
		<div class="text-center imgContainer" id="imgsBoxDiv"
			style="overflow: auto;">
			<c:forEach begin="1" end="${pinum}" var="page">
				<c:if test="${page<10}">
					<img alt="${page}" class="img-responsive pageName${page}"
						src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						data-src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						style="margin: auto; min-height: 64px; min-width: 64px; max-width:100%;">
					<div
						style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 14px;">第${page}页/共${pinum}页
					</div>
				</c:if>
				<c:if test="${page>=10}">
					<img alt="${page}" class="img-responsive pageName${page}"
						src="text/img/demo/playerIcon.png"
						data-src="download/PubDirFile.do?fileid=${fileid}&num=${page-1}"
						style="margin: auto; min-height: 64px; min-width: 64px; max-width: 100%;">
					<div
						style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 14px;">第${page}页/共${pinum}页
					</div>
				</c:if>
			</c:forEach>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			resetImgboxHeight();
			$(window).resize(function() {
				resetImgboxHeight();
			});
			$('#imgsBoxDiv').on('scroll', function() {//当页面滚动的时候绑定事件
				$('.imgContainer img').each(function() {//遍历所有的img标签
					if (checkShow($(this))) {
						$('#currentNumSpan').text($(this).attr("alt"));
						scrollToMinMarkName('.pageName'+$(this).attr("alt"));
						if (!isLoaded($(this))) {
							loadImg($(this));
						}
					}
				})
			})
			$('#imgsMinBoxDiv').on('scroll', function() {//当页面滚动的时候绑定事件
				$('#imgsMinBoxDiv img').each(function() {//遍历所有的img标签
					if (checkShow($(this))) {
						if (!isLoaded($(this))) {
							loadImg($(this));
						}
					}
				})
			})
			//禁止图片拖动下载
			for (i in document.images) {
				document.images[i].ondragstart = imgdragstart;
			}
			try {
				var gallery = new Viewer(document.getElementById('imgsBoxDiv'),
						{
							navbar : true,
							url : 'data-src',
							keyboard : true,
						});
			} catch (e) {
			}
		});
		function resetImgboxHeight() {
			var height = $(window).height() -20 ;
			if (height > 860) {
				height = 860;
			}
			$('#imgsBoxDiv').css('height', height);
			$('#imgsMinBoxDiv').css('height', height);
		}
		//滾動到指定位置
		function scrollToMarkName(markName) {
			var mainContainer = $('#imgsBoxDiv'), //父级容器
			scrollToContainer = mainContainer.find(markName);//指定的class
			mainContainer.animate({
				scrollTop : scrollToContainer.offset().top
						- mainContainer.offset().top
						+ mainContainer.scrollTop()
			}, 0);
		}
		//预览导航
		function scrollToMinMarkName(markName) {
			var mainContainer = $('#imgsMinBoxDiv'), //父级容器
			scrollToContainer = mainContainer.find(markName);//指定的class
			mainContainer.animate({
				scrollTop : scrollToContainer.offset().top
						- mainContainer.offset().top
						+ mainContainer.scrollTop()-$(window).height()/2
			}, 0);
		}

		//图片是否可见
		function checkShow($img) { // 传入一个img的jq对象
			var scrollTop = $(window).scrollTop(); //即页面向上滚动的距离
			var windowHeight = $(window).height(); // 浏览器自身的高度
			var offsetTop = $img.offset().top; //目标标签img相对于document顶部的位置
			if (offsetTop < (scrollTop + windowHeight) && offsetTop > scrollTop) { //在2个临界状态之间的就为出现在视野中的
				return true;
			}
			return false;
		}
		//是否已经加载
		function isLoaded(imgObj) {
			return imgObj.attr('data-src') === imgObj.attr('src'); //如果data-src和src相等那么就是已经加载过了
		}
		//加载图片
		function loadImg(imgObj) {
			imgObj.attr('src', imgObj.attr('data-src')); // 加载就是把自定义属性中存放的真实的src地址赋给src属性
		}
		//不允許拖拽
		function imgdragstart() {
			return false;
		};
	</script>
	<script>
		function customContextMenu(event) {
			event.preventDefault ? event.preventDefault()
					: (event.returnValue = false);
			var cstCM = document.getElementById('cstCM');
			cstCM.style.left = event.clientX + 'px';
			cstCM.style.top = event.clientY + 'px';
			cstCM.style.display = 'block';
			document.onmousedown = clearCustomCM;
		}
		function clearCustomCM() {
			document.getElementById('cstCM').style.display = 'none';
			document.onmousedown = null;
		}
	</script>
</body>
</html>