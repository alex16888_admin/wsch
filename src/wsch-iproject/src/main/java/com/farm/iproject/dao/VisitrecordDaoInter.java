package com.farm.iproject.dao;

import com.farm.iproject.domain.VisitRecord;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;

/* *
 *功能：访问记录数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface VisitrecordDaoInter {
	/**
	 * 删除一个访问记录实体
	 * 
	 * @param entity
	 *            实体
	 */
	public void deleteEntity(VisitRecord visitrecord);

	/**
	 * 由访问记录id获得一个访问记录实体
	 * 
	 * @param id
	 * @return
	 */
	public VisitRecord getEntity(String visitrecordid);

	/**
	 * 插入一条访问记录数据
	 * 
	 * @param entity
	 */
	public VisitRecord insertEntity(VisitRecord visitrecord);

	/**
	 * 获得记录数量
	 * 
	 * @return
	 */
	public int getAllListNum();

	/**
	 * 修改一个访问记录记录
	 * 
	 * @param entity
	 */
	public void editEntity(VisitRecord visitrecord);

	/**
	 * 获得一个session
	 */
	public Session getSession();

	/**
	 * 执行一条访问记录查询语句
	 */
	public DataResult runSqlQuery(DataQuery query);

	/**
	 * 条件删除访问记录实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            删除条件
	 */
	public void deleteEntitys(List<DBRule> rules);

	/**
	 * 条件查询访问记录实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            查询条件
	 * @return
	 */
	public List<VisitRecord> selectEntitys(List<DBRule> rules);

	/**
	 * 条件修改访问记录实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param values
	 *            被修改的键值对
	 * @param rules
	 *            修改条件
	 */
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	/**
	 * 条件合计访问记录:count(*)
	 * 
	 * @param rules
	 *            统计条件
	 */
	public int countEntitys(List<DBRule> rules);

	/**
	 * @param projectid
	 * @return map.put("appid", node[0]); map.put("num", node[1]);
	 *         map.put("POPKEYS", node[2]); map.put("TITLE", node[3]);
	 */
	public List<Map<String, Object>> getHotDoc(String projectid);
}