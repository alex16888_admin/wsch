package com.farm.wuc.events;

import com.farm.core.auth.domain.LoginUser;
import com.farm.parameter.FarmParameterService;

import om.wuc.client.events.WucEventRecorder;
import om.wuc.client.events.domain.WucUser;

public class WucService {

	/**
	 * 获得wuc事件记录服务
	 * 
	 * @return
	 */
	public static WucEventRecorder getRecorder() {
		return WucEventRecorder.getInstance(FarmParameterService.getInstance().getParameter("config.wuc.api.base.url"),
				FarmParameterService.getInstance().getParameter("config.wuc.api.secret"),
				FarmParameterService.getInstance().getParameter("config.wuc.api.loginname"),
				FarmParameterService.getInstance().getParameter("config.wuc.api.password"),
				FarmParameterService.getInstance().getParameterBoolean("config.wuc.event.able"));
	}

	/**
	 * 事件服务是否可用
	 * 
	 * @return
	 */
	public static boolean isEventAble() {
		return FarmParameterService.getInstance().getParameterBoolean("config.wuc.event.able");
	}

	/**
	 * 获得子系统ID
	 * 
	 * @return
	 */
	public static String getWucSysid() {
		return FarmParameterService.getInstance().getParameter("config.wuc.sysid");
	}

	/**
	 * 转换用户
	 * 
	 * @param currentUser
	 * @return
	 */
	public static WucUser getWucUser(LoginUser currentUser) {
		if (currentUser == null) {
			return null;
		}
		WucUser user = new WucUser();
		user.setIp(currentUser.getIp());
		user.setLoginname(currentUser.getLoginname());
		user.setName(currentUser.getName());
		return user;
	}
}
