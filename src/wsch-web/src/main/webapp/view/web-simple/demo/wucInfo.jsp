<%@page import="java.rmi.Naming"%>
<%@page import="java.util.Set"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WUC接口状态-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
</head>
<body>
	<!-- /.carousel -->
	<div class="containerbox">
		<div class="container" style="margin-top: 50px;">
			<div style="margin-top: 58px;"></div>
			<div class="row">
				<div class="col-md-12">
					<h1>
						WUC：接口调用状态：<span style='color: red;'>${message }</span>${isAble?"<span style='color:green;'>成功<span>":"<span style='color:red;'>失败<span>"}</h1>
					<c:if test="${isAble}">
						<button id="sycnWucEventId" onclick="syncWucEventKeys()"
							style="float: right;">同步事件参数（事件模型、关系人类型）</button>
					</c:if>
					<div class="bs-example" data-example-id="striped-table">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>组</th>
									<th>参数</th>
									<th>值</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td rowspan="2">用户同步</td>
									<td>config.wuc.sso.able</td>
									<td><PF:ParameterValue key="config.wuc.sso.able" /></td>
								</tr>
								<tr>
									<td>config.wuc.sso.url</td>
									<td><PF:ParameterValue key="config.wuc.sso.url" /></td>
								</tr>
								<tr>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td rowspan="1">功能状态</td>
									<td>config.wuc.event.able</td>
									<td><PF:ParameterValue key="config.wuc.event.able" /></td>
								</tr>
								<tr>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td rowspan="4">接口参数</td>
									<td>config.wuc.api.base.url</td>
									<td><PF:ParameterValue key="config.wuc.api.base.url" /></td>
								</tr>
								<tr>
									<td>config.wuc.api.secret</td>
									<td><PF:ParameterValue key="config.wuc.api.secret" /></td>
								</tr>
								<tr>
									<td>config.wuc.api.loginname</td>
									<td><PF:ParameterValue key="config.wuc.api.loginname" /></td>
								</tr>
								<tr>
									<td>config.wuc.api.password</td>
									<td><PF:ParameterValue key="config.wuc.api.password" /></td>
								</tr>
								<tr>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>WUC中子系统ID</td>
									<td>config.wuc.sysid</td>
									<td><PF:ParameterValue key="config.wuc.sysid" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div style="margin-top: 288px;"></div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function syncWucEventKeys() {
		var title = $('#sycnWucEventId').text();
		$('#sycnWucEventId').text("同步中...");
		alert('开始同步!!!');
		$.post("demo/registWucEvents.do", {}, function(json) {
			if (json.STATE == 0) {
				alert("成功");
			} else {
				alert(json.MESSAGE);
			}
			$('#sycnWucEventId').text(title);
		}, 'json');
	}
</script>
</html>