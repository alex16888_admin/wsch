package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：组权限类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "PopGroupKey")
@Table(name = "wsch_a_popgourpkey")
public class PopGroupkey implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "GROUPID", length = 32, nullable = false)
        private String groupid;
        @Column(name = "POPKEYID", length = 64, nullable = false)
        private String popkeyid;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;
        @Column(name = "PCONTENT", length = 128)
        private String pcontent;

        public String  getGroupid() {
          return this.groupid;
        }
        public void setGroupid(String groupid) {
          this.groupid = groupid;
        }
        public String  getPopkeyid() {
          return this.popkeyid;
        }
        public void setPopkeyid(String popkeyid) {
          this.popkeyid = popkeyid;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}