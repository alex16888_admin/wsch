package com.farm.iproject.service;

import com.farm.iproject.domain.PopUser;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：权限用户服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface PopuserServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public PopUser insertPopuserEntity(PopUser entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public PopUser editPopuserEntity(PopUser entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deletePopuserEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public PopUser getPopuserEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createPopuserSimpleQuery(DataQuery query);

	/**
	 * @param groupid
	 * @param userid
	 */
	public PopUser addGroupUser(String groupid, String userid);
}