<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--检索记录表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formSearchrecord">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">RESULTID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[32]']" id="entity_resultid"
						name="resultid" value="${entity.resultid}"></td>
				</tr>
				<tr>
					<td class="title">查询时间:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[6]']" id="entity_searchtime"
						name="searchtime" value="${entity.searchtime}"></td>
				</tr>
				<tr>
					<td class="title">时间信息:</td>
					<td colspan="3">${entity.times}</td>
				</tr>
				<tr>
					<td class="title">条件信息:</td>
					<td colspan="3">${entity.rules}</td>
				</tr>
				<tr>
					<td class="title">关键字:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_word"
						name="word" value="${entity.word}"></td>
				</tr>
				<tr>
					<td class="title">时间:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value="${entity.ctime}"></td>
				</tr>
				<tr>
					<td class="title">用户:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_cuser" name="cuser" value="${entity.cuser}"></td>
				</tr>
				<tr>
					<td class="title">结果量:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[5]']" id="entity_size"
						name="size" value="${entity.size}"></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_pstate" name="pstate" value="${entity.pstate}">
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entitySearchrecord" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entitySearchrecord" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formSearchrecord" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionSearchrecord = 'searchrecord/add.do';
	var submitEditActionSearchrecord = 'searchrecord/edit.do';
	var currentPageTypeSearchrecord = '${pageset.operateType}';
	var submitFormSearchrecord;
	$(function() {
		//表单组件对象
		submitFormSearchrecord = $('#dom_formSearchrecord').SubmitForm({
			pageType : currentPageTypeSearchrecord,
			grid : gridSearchrecord,
			currentWindowId : 'winSearchrecord'
		});
		//关闭窗口
		$('#dom_cancle_formSearchrecord').bind('click', function() {
			$('#winSearchrecord').window('close');
		});
		//提交新增数据
		$('#dom_add_entitySearchrecord').bind('click', function() {
			submitFormSearchrecord.postSubmit(submitAddActionSearchrecord);
		});
		//提交修改数据
		$('#dom_edit_entitySearchrecord').bind('click', function() {
			submitFormSearchrecord.postSubmit(submitEditActionSearchrecord);
		});
	});
//-->
</script>