package com.farm.sfile.wdas.util;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class WSClientTest {

	public static void main(String[] args) throws IOException {
		// 1.上传文件（APPID）
		String uuid = UUID.randomUUID().toString();
		String appid = "123112233";
		System.out.println(appid);
		WSClient wdapShare = WSClient.getInstance("sysadmin1", "111111", "F59BD65F7EDAFB087A81D4DCA06C4910",
				"http://127.0.0.1:8081/wdas");
		wdapShare.uploadFile(new File("D:\\SVN\\资源素材\\pdf\\易中天中华史-国家.pdf"), "易中天中华史-国家.pdf", appid, uuid,
				new WSFileHttpUploads.ProgressHandle() {
					public void handle(int percent, long allsize, long completesize, String state) {
						System.out.println(state + ":" + percent);  
					}
				});
		for (int n = 1; n < 3; n++) {
			// 3.下载用于播放的地址（APPID）（传输中/不存在/无预览文件）
			System.out.println("下載" + wdapShare.getFileDownloadUrl(appid));
			// 4.下载用于下载的地址（APPID）（传输中/不存在/无预览文件）
			System.out.println("播放" + wdapShare.getFilePlayUrl(appid));
			// 5.下载用于預覽的地址（APPID）（传输中/不存在/无预览文件）
			System.out.println("预览" + wdapShare.getFileViewUrl(appid));
		}
		System.out.println("远程副本数量:" + wdapShare.getFilenum(appid));
		// 2.删除文件（APPID）
		// wdapShare.delFile(appid);
	}

}
