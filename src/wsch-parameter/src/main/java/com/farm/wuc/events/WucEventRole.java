package com.farm.wuc.events;

/**
 * 关系人模型
 * 
 * @author macpl
 *
 */
public enum WucEventRole {

	OPRETOR("操作人");

	private String key;
	private String title;
	private String note;

	private WucEventRole(String title) {
		this.key = this.name();
		this.title = title;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
