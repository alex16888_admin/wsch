# WSCH鉴权内容检索系统

#### 介绍
wsch提供基于内容的全文检索功能，您可以将任何已有系统的内容数据通过接口提交至wsch中，并可以与wsch的用户绑定权限，由wsch提供基于权限的全文检索功能，并在检索结果页中将检索到的结果条目绑定指向外部业务系统中。

- 全文检索
- 支持带权限的检索，不同登录用户绑定不同检索内容
- 支持可配置的多字段域查询
- 同一个检索项目可对接多个业务系统，汇总多个系统的内容查询
- 支持对内容进行多级分类索引
- 配置功能支持通过界面或接口进行配置
- 索引支持界面或接口进行提交


#### 软件架构

    jdk8
    lucene8
    maven
    spring4
    spring-mvc4
    hibernate4
    bootstrap
    tomcat7
    mysql

#### 安装教程

1.  下载源码
2.  安装源码
3.  安装数据库
4.  启动web服务

视频教程地址【[http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837f21ee11018003efb5bc166f](http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837f21ee11018003efb5bc166f)】

#### 使用说明

1.  配置检索项目
2.  添加内容索引（通过接口API）
3.  添加用户内容权限（通过接口API）
4.  用户登录后检索

视频教程地址【[http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837f21ee11018003efb5bc166f](http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837f21ee11018003efb5bc166f)】

#### 截图

1.  首页
![输入图片说明](resource/imgs/%E9%A6%96%E9%A1%B5%E6%88%AA%E5%9B%BE.png)
2.  检索页
![输入图片说明](resource/imgs/%E6%A3%80%E7%B4%A2%E6%88%AA%E5%9B%BE.png)
3.  快照
![输入图片说明](resource/imgs/%E5%BF%AB%E7%85%A7.png)
### 开源项目
	
> WCP:知识管理系统 [https://gitee.com/macplus/WCP](https://gitee.com/macplus/WCP)

> WDA:文件转换组件（附件在线预览）[https://gitee.com/macplus/WDA](https://gitee.com/macplus/WDA)

> WTS:在线答题系统 [https://gitee.com/macplus/WTS](https://gitee.com/macplus/WTS)

> WLP:在线学习系统 [https://gitee.com/macplus/WLP](https://gitee.com/macplus/WLP)

> PLOGS:项目任务日志管理系统 [https://gitee.com/macplus/plogs](https://gitee.com/macplus/plogs)

> WSCH:鉴权内容检索系统 [https://gitee.com/macplus/wsch](https://gitee.com/macplus/wsch)

## 商业版产品介绍

> 知识库/在线答題/在线学习产品介绍 [http://www.wcpknow.com/home/index.html](http://www.wcpknow.com/home/index.html)
