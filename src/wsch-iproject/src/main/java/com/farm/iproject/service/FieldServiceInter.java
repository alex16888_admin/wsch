package com.farm.iproject.service;

import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Project;
import com.farm.wsch.index.pojo.IndexDocument;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目字段服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FieldServiceInter {

	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Field insertFieldEntity(Field entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Field editFieldEntity(Field entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFieldEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Field getFieldEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createFieldSimpleQuery(DataQuery query);

	/**
	 * 获得项目索引域数量
	 * 
	 * @param projectid
	 * @return
	 */
	public int getFieldsNum(String projectid);

	/**
	 * 域keyid是否有重复的
	 * 
	 * @param keyid
	 * @param id
	 * @return
	 */
	public boolean isKeyidRepeat(String keyid, String id, String projectid);

	/**
	 * 獲得項目所有索引域
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Field> getFields(String projectid);

	/**
	 * 創建默認字段
	 * 
	 * @param project
	 * @param user
	 */
	public void initProjectFields(Project project, LoginUser user);

	/**
	 * 获得索引文档
	 * 
	 * @param projectid
	 * @param map
	 * @param appkeyid
	 * @return
	 */
	public IndexDocument getIndexDocument(String projectid, String appkeyid, Map<String, Object> map);

	/**
	 * 获得项目的检索字段
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Field> getSearchField(String projectid);

	/**
	 * 格式化检索字段条件
	 * 
	 * @param projectid
	 * @param fieldkeyid
	 * @return
	 */
	public List<String> formatSearchField(String projectid, String fieldkeyid);

	/**
	 * 获得检索权重
	 * 
	 * @param projectid
	 * @param fieldKeys
	 * @return
	 */
	public Map<String, Float> getProjectFieldBoost(String projectid, List<String> fieldKeys);

	public Map<String, Field> getProjectFields(String projectid);

	/**
	 * 获得排序域
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Field> getSortField(String projectid);

	/**
	 * 获得一个域定义
	 * 
	 * @param projectid
	 * @param key
	 * @return
	 */
	public Field getFieldEntity(String projectid, String key);

}