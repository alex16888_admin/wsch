<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="DicTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="DicTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="DicTypeTree"></ul>
	</div>
	<div data-options="region:'north',border:false">
		<form id="searchDictypeForm">
			<table class="editTable">
				<tr>
					<td class="title">检索项目:</td>
					<td><input name="PROJECTID:=" type="hidden"
						value="${project.id }">${project.name}</td>
					<td class="title">上级字典:</td>
					<td><input name="PARENTID:=" id="limitParentid" type="hidden">
						<input id="parentTitleid" type="text"
						style="width: 80px; background-color: #eeeeee;"
						readonly="readonly"></td>
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text" style="width: 80px;"></td>
					<td colspan="2"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataDictypeGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="SUPERNAME" data-options="sortable:false" width="40">字典</th>
					<th field="NAME" data-options="sortable:true" width="40">名称</th>
					<th field="KEYID" data-options="sortable:true" width="40">KEY</th>
					<th field="SORT" data-options="sortable:true" width="20">排序</th>
					<th field="FIELDKEY" data-options="sortable:true" width="40">检索域</th>
					<th field="LIKETYPE" data-options="sortable:true" width="40">匹配模式</th>
					<th field="ABLETYPE" data-options="sortable:true" width="40">启用类型</th>

				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionDictype = "dictype/del.do";//删除URL
	var url_formActionDictype = "dictype/form.do";//增加、修改、查看URL
	var url_searchActionDictype = "dictype/query.do?projectid=${project.id }";//查询URL
	var title_windowDictype = "字典类型管理";//功能名称
	var gridDictype;//数据表格对象
	var searchDictype;//条件查询组件对象
	var toolBarDictype = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataDictype
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataDictype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataDictype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataDictype
	} ];
	$(function() {
		//初始化数据表格
		gridDictype = $('#dataDictypeGrid').datagrid({
			url : url_searchActionDictype,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarDictype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchDictype = $('#searchDictypeForm').searchForm({
			gridObj : gridDictype
		});
		$('#DicTypeTree').tree({
			url : 'dictype/dictypeTree.do?projectid=${project.id }',
			onSelect : function(node) {
				$('#limitParentid').val(node.id);
				$('#parentTitleid').val(node.text);
				searchDictype.dosearch({
					'ruleText' : searchDictype.arrayStr()
				});
			}
		});
		$('#DicTypeTreeReload').bind('click', function() {
			$('#DicTypeTree').tree('reload');
		});
		$('#DicTypeTreeOpenAll').bind('click', function() {
			$('#DicTypeTree').tree('expandAll');
		});
	});
	//查看
	function viewDataDictype() {
		var selectedArray = $(gridDictype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionDictype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winDictype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataDictype() {
		var parentid = $('#limitParentid').val();
		var url = url_formActionDictype + '?parentid=' + parentid
				+ '&projectid=${project.id}&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winDictype',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataDictype() {
		var selectedArray = $(gridDictype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionDictype + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winDictype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataDictype() {
		var selectedArray = $(gridDictype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridDictype).datagrid('loading');
					$.post(url_delActionDictype + '?ids='
							+ $.farm.getCheckedIds(gridDictype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridDictype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridDictype).datagrid('reload');
									$.messager.confirm('确认对话框',
											'数据更新,是否重新加载左侧树？', function(r) {
												if (r) {
													$('#DicTypeTree').tree(
															'reload');
												}
											});
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>