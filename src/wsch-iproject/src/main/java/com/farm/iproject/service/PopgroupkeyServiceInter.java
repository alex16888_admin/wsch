package com.farm.iproject.service;

import com.farm.iproject.domain.PopGroupkey;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：组权限服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface PopgroupkeyServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public PopGroupkey insertPopgroupkeyEntity(PopGroupkey entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public PopGroupkey editPopgroupkeyEntity(PopGroupkey entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deletePopgroupkeyEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public PopGroupkey getPopgroupkeyEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createPopgroupkeySimpleQuery(DataQuery query);

	/**
	 * @param groupid
	 * @param popkeyid
	 */
	public PopGroupkey addGroupKey(String groupid, String popkeyid);

	/**
	 * 添加组权限
	 * 
	 * @param gourpid
	 * @param parseIds
	 */
	public void addGroupKey(String groupid, List<String> parseIds);
}