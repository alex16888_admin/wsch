package com.farm.iproject.controller;

import com.farm.iproject.domain.Pdocument;
import com.farm.iproject.service.PdocumentServiceInter;
import com.farm.iproject.utils.IndexRuner;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.io.File;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：业务文档控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/pdocument")
@Controller
public class PdocumentController extends WebUtils {
	private final static Logger log = Logger.getLogger(PdocumentController.class);
	@Resource
	private PdocumentServiceInter pDocumentServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String projectid, DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("projectid", projectid, "="));
			DataResult result = pDocumentServiceImpl.createPdocumentSimpleQuery(query).search();
			result.runformatTime("CTIME", " yyyy-MM-dd HH:mm");
			result.runDictionary("1:暂存,2:等待提交,3:完成索引,0:错误", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				pDocumentServiceImpl.deletePdocumentEntity(id, getCurrentUser(session), true);
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/delall")
	@ResponseBody
	public Map<String, Object> delall(String projectid, HttpSession session) {
		try {
			pDocumentServiceImpl.deleteAllIndex(projectid, null, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 执行批次索引数据
	 * 
	 * @return
	 */
	@RequestMapping("/runindex")
	@ResponseBody
	public Map<String, Object> runindex(Boolean more, String ids, HttpSession session) {
		try {
			String taskkey = null;
			String projectid = null;
			for (String id : parseIds(ids)) {
				Pdocument document = pDocumentServiceImpl.getPdocumentEntity(id);
				taskkey = document.getTaskkey();
				projectid = document.getProjectid();
				break;
			}
			if (more != null && !more) {
				pDocumentServiceImpl.runIndexBySingle(taskkey, projectid);
			} else {
				pDocumentServiceImpl.runIndex(taskkey, projectid);
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/runAllindex")
	@ResponseBody
	public Map<String, Object> runAllindex(String projectid, HttpSession session) {
		try {
			IndexRuner ir = IndexRuner.getInstance();
			ir.run(projectid);
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/getRunProcess")
	@ResponseBody
	public Map<String, Object> getRunProcess(HttpSession session) {
		try {
			IndexRuner ir = IndexRuner.getInstance();
			return ViewMode.getInstance().putAttr("process", ir.getProcess()).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String projectid, HttpSession session) {
		return ViewMode.getInstance().putAttr("projectid", projectid).returnModelAndView("iproject/PdocumentResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				ViewMode view = ViewMode.getInstance();
				Pdocument doc = pDocumentServiceImpl.getPdocumentEntity(ids);
				Map<String, Object> docmap = pDocumentServiceImpl.getDocMap(doc);
				view.putAttr("docmap", docmap);
				return view.putAttr("pageset", pageset).putAttr("entity", doc)
						.returnModelAndView("iproject/PdocumentForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("iproject/PdocumentForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", pDocumentServiceImpl.getPdocumentEntity(ids))
						.returnModelAndView("iproject/PdocumentForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/PdocumentForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/PdocumentForm");
		}
	}
}
