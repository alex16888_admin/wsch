package com.farm.web.tag;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

import com.farm.authority.service.PopServiceInter;
import com.farm.iproject.service.WeburlServiceInter;
import com.farm.sfile.enums.FileModel;
import com.farm.sfile.service.FileBaseServiceInter;
import com.farm.util.spring.BeanFactory;

public class WebUrls extends SimpleTagSupport {
	private String var;
	private String type;
	private final static WeburlServiceInter WEBUSER_SERVER = (WeburlServiceInter) BeanFactory
			.getBean("weburlServiceImpl");
	private final static FileBaseServiceInter fileBaseServiceImpl = (FileBaseServiceInter) BeanFactory
			.getBean("fileBaseServiceImpl");
	private static final Logger log = Logger.getLogger(WebUrls.class);

	@Override
	public void doTag() throws JspException, IOException {
		PageContext page = (PageContext) this.getJspContext();
		List<Map<String, Object>> urls = WEBUSER_SERVER.getWebUrlList();
		// 进行迭代
		for (Map<String, Object> node : urls) {
			if (node.get("TYPE").equals(type)) {
				getJspContext().setAttribute(var, node);
				// 输出标签体
				getJspBody().invoke(null);
			}
		}
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
