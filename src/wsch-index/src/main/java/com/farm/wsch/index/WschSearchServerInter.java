package com.farm.wsch.index;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.queryparser.classic.ParseException;

import com.farm.wsch.index.pojo.IndexResult;
import com.farm.wsch.index.pojo.ResultDicNum;
import com.farm.wsch.index.pojo.SearchRule;

public interface WschSearchServerInter {

	/**
	 * 添加一個查詢條件
	 * 
	 * @param value
	 * @param titles
	 * @param boots
	 * @return
	 * @throws ParseException
	 */
	public WschSearchServerInter addRule(String value, List<String> titles, Map<String, Float> boots, boolean isSmart)
			throws ParseException;

	/**
	 * 添加一个查询条件
	 * 
	 * @param rule
	 * @return
	 * @throws ParseException
	 */
	public WschSearchServerInter addRule(SearchRule rule) throws ParseException;

	/**
	 * 添加一個查詢條件
	 * 
	 * @param value
	 * @param title
	 * @param isLike  是否模糊查询
	 * @param isSmart 是否原子条件（原子条件不对检索词分词）
	 * @return
	 * @throws ParseException
	 */
	public WschSearchServerInter addRule(String value, String title, boolean isLike, boolean isSmart)
			throws ParseException;

	/**
	 * 查詢結果集合
	 * 
	 * @param pagenum
	 * @param pagesize
	 * @return
	 * @throws IOException
	 */
	public IndexResult search(int pagenum, int pagesize) throws IOException;

	/**
	 * 设置结果统计对象，在字典中记录每个字典的匹配次数
	 * 
	 * @param dics
	 */
	public void setResultDicNums(List<ResultDicNum> dics);

	/**
	 * 设置在结果中查询
	 * 
	 * @param resultid
	 */
	public void setFilterResultId(String resultid);

	/**
	 * 设置用户拥有检索权限的key
	 * 
	 * @param popkeys
	 */
	public void setSearchPopKeys(Set<String> popkeys);

	/**
	 * 设置排序条件
	 * 
	 * @param sortkey
	 * @param sorttype
	 */
	public void setSort(String sortkey, String sorttype);

}
