package com.farm.iproject.service;

import com.farm.iproject.domain.Virtualfield;
import com.farm.wsch.index.pojo.IndexResult;
import com.farm.core.sql.query.DataQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：虚拟字段服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface VirtualfieldServiceInter {
	/**
	 * 域类型
	 * 
	 * @author macpl
	 *
	 */
	public enum SearchType {
		// 标题，正文，内容图，标签，时间，作者等
		NONE("无"), TITLE("标题"), CONTENT("正文"), IMG("展示图"), TAG("标签"), TIME("时间"), AUTHOR("作者"), OLINK("原文地址"),
		CATALOG("类型分类");

		String title;
		String key;

		SearchType(String title) {
			this.title = title;
			this.key = this.name();
		}

		public static List<SearchType> getList() {
			List<SearchType> list = new ArrayList<VirtualfieldServiceInter.SearchType>();
			for (SearchType field : values()) {
				list.add(field);
			}
			return list;
		}

		public static Map<String, String> getDic() {
			Map<String, String> map = new HashMap<String, String>();
			for (SearchType field : values()) {
				map.put(field.getKey(), field.getTitle());
			}
			return map;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}

	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Virtualfield insertVirtualfieldEntity(Virtualfield entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Virtualfield editVirtualfieldEntity(Virtualfield entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteVirtualfieldEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Virtualfield getVirtualfieldEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createVirtualfieldSimpleQuery(DataQuery query);

	/**
	 * 加载虚拟字段
	 * 
	 * @param result
	 * @param projectid
	 * @param appkeyid
	 */
	public void initVirtualField(IndexResult result, String projectid);

	/**
	 * 加载虚拟字段
	 * 
	 * @param result
	 * @param projectid
	 * @param appkeyid
	 */
	public void initVirtualField(Map<String, Object> document, Map<String, List<Virtualfield>> VirtualFields);

	/**
	 * 获得所有虚拟字段
	 * 
	 * @param projectid
	 * @return <appKeyid,List<Virtualfield>>
	 */
	public Map<String, List<Virtualfield>> getAllVirtualField(String projectid);

	/**
	 * 获得虚拟域
	 * 
	 * @param projectid 项目id
	 * @param appkey    检索业务key
	 * @param type      特殊类型
	 * @return
	 */
	public Virtualfield getVirtualfield(String projectid, String appkey, String type);

}