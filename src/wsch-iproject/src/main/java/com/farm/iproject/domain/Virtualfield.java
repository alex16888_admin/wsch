package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：虚拟字段类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "VirtualField")
@Table(name = "wsch_p_virtualfield")
public class Virtualfield implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "APPMODELID", length = 32, nullable = false)
        private String appmodelid;
        @Column(name = "TYPE", length = 64, nullable = false)
        private String type;
        @Column(name = "FORMULA", length = 1024, nullable = false)
        private String formula;
        @Column(name = "NAME", length = 128, nullable = false)
        private String name;

        public String  getAppmodelid() {
          return this.appmodelid;
        }
        public void setAppmodelid(String appmodelid) {
          this.appmodelid = appmodelid;
        }
        public String  getType() {
          return this.type;
        }
        public void setType(String type) {
          this.type = type;
        }
        public String  getFormula() {
          return this.formula;
        }
        public void setFormula(String formula) {
          this.formula = formula;
        }
        public String  getName() {
          return this.name;
        }
        public void setName(String name) {
          this.name = name;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}