package com.farm.iproject.service;

import com.farm.iproject.domain.PopGroup;
import com.farm.core.sql.query.DataQuery;

import java.util.Set;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：权限组服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface PopgroupServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public PopGroup insertPopgroupEntity(PopGroup entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public PopGroup editPopgroupEntity(PopGroup entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deletePopgroupEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public PopGroup getPopgroupEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createPopgroupSimpleQuery(DataQuery query);

	/**
	 * 获得用户的检索权限key
	 * 
	 * @param projectid
	 * @param object
	 * @return
	 */
	public Set<String> getUserSearchKeys(String projectid, String userid);

	/**
	 * 删除项目权限组
	 * 
	 * @param projectid
	 * @param groupname 为空时删除项目所有权限组
	 */
	public void deletePopgourps(String projectid, String groupname, LoginUser user);

	/**
	 * 获得一个权限组
	 * 
	 * @param projectKey
	 * @param groupname
	 * @return
	 */
	public PopGroup getPopgroupByName(String projectKey, String groupname);
}