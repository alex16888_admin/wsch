<%@page import="com.farm.parameter.service.impl.ConstantVarService"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@page import="com.farm.util.spring.BeanFactory"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title><PF:ParameterValue key="config.sys.title" /></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/view/web-simple/atext/include-web.jsp"></jsp:include>
</head>
<body>
	<div class="container" style="padding-top: 20px;">
		<div
			style=" text-align: center; padding-bottom: 20px;">
			<img style="max-width: 128px; max-height: 128px;"
				src="<PF:basePath/>webfile/Publogo.do" alt="..." class="img-rounded">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group" style="text-align: center;">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<jsp:include page="licenceInfo.jsp"></jsp:include>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</div>
		</div>
		<div
			style="padding-top: 20px; padding-bottom: 50px; text-align: center;">
			<a style="font-size: 24px; color: #666666;" href="<PF:basePath/>">点击进入-系统首页</a>
		</div>
		<div class="row">
			<div class="col-md-12 text-center" style="color: #b7b8b8;">
				<hr />
				<a href="<PF:basePath/>"><PF:ParameterValue
						key="config.sys.foot" /></a> -V
				<PF:ParameterValue key="config.sys.version" />
			</div>
		</div>
	</div>
</body>
</html>