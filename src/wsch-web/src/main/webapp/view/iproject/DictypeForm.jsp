<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--字典类型表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formDictype">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">项目:</td>
					<td>${project.name}</td>
					<td class="title">上级字典:</td>
					<td><c:if test="${parent==null }">无</c:if> <c:if
							test="${parent!=null }">${parent.name }</c:if>
						<div>
							<input type="hidden" name="parentid" value="${parent.id}">
						</div>
						<div>
							<input type="hidden" name="superid" value="${entity.superid}">
						</div>
						<div>
							<input type="hidden" name="projectid" value="${project.id}">
						</div></td>
				</tr>
				<c:if test="${parent==null }">
					<tr>
						<td class="title">字段域KEY:</td>
						<td ><select name="fieldkey" id="entity_fieldkey"
							val="${entity.fieldkey}">
								<c:forEach items="${fields}" var="node">
									<option value="${node.keyid}">${node.keyid}:${node.name}</option>
								</c:forEach>
						</select></td>
						<td class="title">字典key:</td>
						<td ><input type="text" style="width: 120px;"
							class="easyui-validatebox"
							data-options="required:true,validType:[,'maxLength[32]']"
							id="entity_keyid" name="keyid" value="${entity.keyid}"></td>
					</tr>
				</c:if>
				<c:if test="${parent==null }">
					<tr>
						<td class="title">匹配模式:</td>
						<td><select name="liketype" id="entity_liketype"
							val="${entity.liketype}">
								<option value="1">完全匹配</option>
								<option value="2">匹配子集</option>
						</select></td>
						<td class="title">是否合计:</td>
						<td><select name="sumable" id="entity_sumable"
							val="${entity.sumable}">
								<option value="1">数合计</option>
								<option value="2">数不合计</option>
						</select></td>
					</tr>
				</c:if>
				<c:if test="${parent==null }">
					<tr>
						<td class="title">启用类型:</td>
						<td><select name="abletype" id="entity_abletype"
							val="${entity.abletype}"><option value="1">结果</option>
								<option value="2">条件/结果</option></select></td>
						<td class="title">状态:</td>
						<td><select name="pstate" id="entity_pstate"
							val="${entity.pstate}"><option value="1">可用</option>
								<option value="0">禁用</option></select></td>
					</tr>
				</c:if>
				<tr>
					<td class="title">字典名称:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
				</tr>
				<c:if test="${parent!=null }">
					<tr>
						<td class="title">字典key:</td>
						<td colspan="3"><input type="text" style="width: 120px;"
							class="easyui-validatebox"
							data-options="required:true,validType:[,'maxLength[32]']"
							id="entity_keyid" name="keyid" value="${entity.keyid}"></td>
					</tr>
				</c:if>
				<c:if test="${parent==null }">
					<input type="hidden" id="entity_keyid" name="keyid"
						value="${entity.keyid}">
				</c:if>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityDictype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityDictype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formDictype" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionDictype = 'dictype/add.do';
	var submitEditActionDictype = 'dictype/edit.do';
	var currentPageTypeDictype = '${pageset.operateType}';
	var submitFormDictype;
	$(function() {
		//表单组件对象
		submitFormDictype = $('#dom_formDictype').SubmitForm({
			pageType : currentPageTypeDictype,
			grid : gridDictype,
			currentWindowId : 'winDictype'
		});
		//关闭窗口
		$('#dom_cancle_formDictype').bind('click', function() {
			$('#winDictype').window('close');
		});
		//提交新增数据
		$('#dom_add_entityDictype').bind(
				'click',
				function() {
					submitFormDictype.postSubmit(submitAddActionDictype,
							function(flag) {
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#DicTypeTree')
														.tree('reload');
											}
										});
								return true;
							});
				});
		//提交修改数据
		$('#dom_edit_entityDictype').bind('click', function() {
			submitFormDictype.postSubmit(submitEditActionDictype, function() {
				$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？', function(r) {
					if (r) {
						$('#OrganizationTree').tree('reload');
					}
				});
				return true;
			});
		});
	});
//-->
</script>