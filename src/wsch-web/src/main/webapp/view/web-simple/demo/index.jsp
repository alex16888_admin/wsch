<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>测试环境- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
</head>
<body>
	<!-- /.carousel -->
	<div class="containerbox">
		<div class="container" style="min-height: 500px; margin-top: 50px;">
			<div style="margin-top: 8px;"></div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">测试面板</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li role="presentation" class="active"><a
									href="demo/wucinfo.do">同步wuc事件配置</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#InitTypePop').click(function() {
				if (confirm("该操作有一定风险是否继续执行？")) {
					$('#InitTypePop').addClass("disabled");
					$('#InitTypePop').text("提交中...");
					window.location = basePath + "demo/initTypePop.do";
				}
			});
		});
	</script>
</body>
</html>