<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div style="text-align: center; padding-top: 20px;">

	<table
		style="width: 500px; font-size: 12px; color: #666666; margin: auto;">
		<tr>
			<td><img alt="" src="<PF:basePath/>text/img/database.png"
				style="max-width: 128px;"></td>
			<td ><div
					style="margin-left: 20px; border: 1px dashed #ffffff; padding: 8px; background-color: #ffffff; border-radius: 12px;">
					<div>接口参数</div>
					<div style="text-align: left;height: 100px;overflow: auto;">
						<div style="padding: 20px;">
							projectid:${project.id},<br />jsonstr:{,<br />
							<c:forEach items="${fields}" var="field">
				&nbsp;&nbsp;	${field.keyid }&nbsp;&nbsp;:&nbsp;&nbsp;${field.type }&nbsp;${field.name },<br />
							</c:forEach>
							}
						</div>
					</div>
				</div></td>
		</tr>
	</table>

	<div
		style="text-align: center; margin: auto; margin-top: 20px; width: 400px;">
		<table class="easyui-datagrid" style="height: 250px; margin: auto;"
			id="datashowId" data-options="fitColumns:true,singleSelect:true">
			<thead>
				<tr>
					<th data-options="field:'code',width:100">属性</th>
					<th data-options="field:'name',width:300">值</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>项目名称</td>
					<td>${project.name }</td>
				</tr>
				<tr>
					<td>项目ID</td>
					<td>${project.id }</td>
				</tr>
				<tr>
					<td>KEYID</td>
					<td>${project.keyid }</td>
				</tr>
				<tr>
					<td>索引量</td>
					<td>${info.NUMDOCS }</td>
				</tr>
				<tr>
					<td>索引文件地址</td>
					<td>${info.PATH }</td>
				</tr>
				<tr>
					<td>操作</td>
					<td><a id="btn" href="javascript:addDataCreatIndex()"
						class="easyui-linkbutton" data-options="iconCls:'icon-add'">添加索引</a>
						<a id="btn" href="javascript:listDataIndex()"
						class="easyui-linkbutton"
						data-options="iconCls:'icon-address-book-open'">索引记录</a><a
						id="btn" href="javascript:mergeIndex()" class="easyui-linkbutton"
						data-options="iconCls:'	icon-document-library'">索引合并</a></td>
				</tr>
			</tbody>
		</table>

	</div>
</div>
<script type="text/javascript">
	$(function() {
		var gridProject = $('#datashowId').datagrid();
		$('.easyui-linkbutton').linkbutton();
	});
	//新增
	function addDataCreatIndex() {
		var url = 'indexc/creatForm.do?projectid=${project.id }';
		$.farm.openWindow({
			id : 'winAppmodel',
			width : 600,
			height : 400,
			modal : true,
			url : url,
			title : '添加索引'
		});
	}
	//索引记录
	function listDataIndex() {
		var url = 'pdocument/list.do?projectid=${project.id}';
		$.farm.openWindow({
			id : 'winIndexDataList',
			width : 800,
			height : 400,
			modal : true,
			url : url,
			title : '索引記錄'
		});
	}
	//合并索引
	function mergeIndex() {
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认合并索引", function(flag) {
			if (flag) {
				$.post('indexc/mergeIndex.do?projectid=${project.id}', {},
						function(flag) {
							var jsonObject = JSON.parse(flag, null);
							if (jsonObject.STATE == 0) {
								$.messager.alert(MESSAGE_PLAT.SUCCESS,
										"索引合并完成", 'info');
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ jsonObject.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						});
			}
		});
	}
</script>
