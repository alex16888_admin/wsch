<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>访问记录数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<!-- <div data-options="region:'north',border:false">
		<form id="searchVisitrecordForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div> -->
	<div data-options="region:'center',border:false">
		<table id="dataVisitrecordGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="TITLE" data-options="sortable:true" width="80">标题</th>
					<th field="CTIME" data-options="sortable:true" width="20">时间</th>
					<th field="USERNAME" data-options="sortable:true" width="20">用户</th>
					<th field="APPID" data-options="sortable:true" width="20">APPID</th>
					<th field="RESULTID" data-options="sortable:true" width="20">RESULTID</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionVisitrecord = "visitrecord/del.do";//删除URL
	var url_formActionVisitrecord = "visitrecord/form.do";//增加、修改、查看URL
	var url_searchActionVisitrecord = "visitrecord/query.do";//查询URL
	var title_windowVisitrecord = "访问记录管理";//功能名称
	var gridVisitrecord;//数据表格对象
	var searchVisitrecord;//条件查询组件对象
	var toolBarVisitrecord = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataVisitrecord
	},
	//{
	//	id : 'add',
	//	text : '新增',
	//	iconCls : 'icon-add',
	//	handler : addDataVisitrecord
	//}, {
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataVisitrecord
	//}, 
	{
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataVisitrecord
	} ];
	$(function() {
		//初始化数据表格
		gridVisitrecord = $('#dataVisitrecordGrid').datagrid({
			url : url_searchActionVisitrecord,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarVisitrecord,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchVisitrecord = $('#searchVisitrecordForm').searchForm({
			gridObj : gridVisitrecord
		});
	});
	//查看
	function viewDataVisitrecord() {
		var selectedArray = $(gridVisitrecord).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionVisitrecord + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winVisitrecord',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataVisitrecord() {
		var url = url_formActionVisitrecord + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winVisitrecord',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataVisitrecord() {
		var selectedArray = $(gridVisitrecord).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionVisitrecord + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winVisitrecord',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataVisitrecord() {
		var selectedArray = $(gridVisitrecord).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridVisitrecord).datagrid('loading');
					$.post(url_delActionVisitrecord + '?ids='
							+ $.farm.getCheckedIds(gridVisitrecord, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridVisitrecord).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridVisitrecord).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>