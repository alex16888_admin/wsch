package com.farm.iproject.service.impl;

import com.farm.iproject.domain.PopGroup;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.PopgroupDaoInter;
import com.farm.iproject.dao.PopgroupkeyDaoInter;
import com.farm.iproject.dao.PopuserDaoInter;
import com.farm.iproject.service.PopgroupServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：权限组服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class PopgroupServiceImpl implements PopgroupServiceInter {
	@Resource
	private PopgroupDaoInter popgroupDaoImpl;
	@Resource
	private PopgroupkeyDaoInter popgroupkeyDaoImpl;
	@Resource
	private PopuserDaoInter popuserDaoImpl;
	private static final Logger log = Logger.getLogger(PopgroupServiceImpl.class);

	@Override
	@Transactional
	public PopGroup insertPopgroupEntity(PopGroup entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return popgroupDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public PopGroup editPopgroupEntity(PopGroup entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		PopGroup entity2 = popgroupDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setPstate(entity.getPstate());
		// entity2.setCtime(entity.getCtime());
		entity2.setGroupname(entity.getGroupname());
		entity2.setPcontent(entity.getPcontent());
		entity2.setProjectid(entity.getProjectid());
		entity2.setId(entity.getId());
		popgroupDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deletePopgroupEntity(String id, LoginUser user) {
		popuserDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("GROUPID", id, "=")).toList());
		popgroupkeyDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("GROUPID", id, "=")).toList());
		popgroupDaoImpl.deleteEntity(popgroupDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public PopGroup getPopgroupEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return popgroupDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createPopgroupSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WSCH_A_POPGOURP a LEFT JOIN WSCH_A_POPUSER b on b.GROUPID=a.ID  left join WSCH_A_POPGOURPKEY c on c.GROUPID=a.ID left join alone_auth_user d on d.id=b.USERID left join wsch_p_project e on a.PROJECTID=e.ID",
				"a.GROUPNAME as GROUPNAME,a.ID as ID,a.PCONTENT as PCONTENT,a.PROJECTID as PROJECTID,a.PSTATE as PSTATE,e.NAME as PROJECTNAME,a.CTIME as CTIME ");
		dbQuery.setDistinct(true);
		return dbQuery;
	}

	@Override
	@Transactional
	public Set<String> getUserSearchKeys(String projectid, String userid) {
		if (StringUtils.isBlank(userid)) {
			userid = "ANONYMOUS";
		}
		return popgroupDaoImpl.getUserSearchKeys(projectid, userid);
	}

	@Override
	@Transactional
	public void deletePopgourps(String projectid, String groupname, LoginUser user) {
		DBRuleList dbs = DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "="));
		if (StringUtils.isNotBlank(groupname)) {
			dbs.add(new DBRule("GROUPNAME", groupname, "like-"));
		}
		List<PopGroup> popgourps = popgroupDaoImpl.selectEntitys(dbs.toList());
		for (PopGroup gourp : popgourps) {
			deletePopgroupEntity(gourp.getId(), user);
		}
	}

	@Override
	@Transactional
	public PopGroup getPopgroupByName(String projectid, String groupname) {
		DBRuleList dbs = DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "="));
		dbs.add(new DBRule("GROUPNAME", groupname, "="));
		List<PopGroup> popgourps = popgroupDaoImpl.selectEntitys(dbs.toList());
		if (popgourps.size() > 0) {
			return popgourps.get(0);
		} else {
			return null;
		}
	}

}
