<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${cproject.name }-检索</title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<link
	href="<PF:basePath/>text/lib/bootstrap-treeview/bootstrap-treeview.min.css"
	rel="stylesheet">
<script
	src="<PF:basePath/>text/lib/bootstrap-treeview/bootstrap-treeview.min.js"></script>
</head>
<style>
<!--
.high-light {
	color: #f73131;
}

.wsch-searchinfo {
	color: #999999;
	padding-bottom: 10px;
}

.wsch-result {
	padding-bottom: 20px;
}

.wsch-result-title {
	font-size: 18px;
	color: #666666;
	font-weight: 400;
	overflow-wrap: break-word;
	padding-bottom: 8px;
}

.wsch-result-title a {
	color: #3650b9;
	text-decoration: underline;
}

.wsch-result-title a:HOVER {
	color: #315efb;
}

.wsch-result-content {
	font-size: 13px;
	line-height: 1.7em;
}

.wsch-result-tags {
	font-size: 13px;
	color: #999999;
}

.wsch-result-tags .label {
	font-size: 13px;
	padding: 4px;
	font-weight: 200;
	padding-top: 2px;
	padding-bottom: 2px;
}

.wsch-result-tags .label .high-light {
	color: #ffa0a0;
}

.wsch-result-author {
	font-size: 13px;
	color: #999999;
	margin-right: 20px;
}

.wsch-result-time {
	font-size: 13px;
	color: #999999;
	margin-right: 20px;
}

.wsch-result-catalog {
	font-size: 13px;
	color: #999999;
	margin-right: 20px;
}

.list-group {
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0);
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0);
}

.list-group .treeNum {
	margin-left: 4px;
	color: red;
}

.list-group-item {
	padding-top: 6px;
	padding-bottom: 6px;
}
-->
</style>
<body>
	<div class="container-fluid">
		<div class="row">
			<!-- 导航 -->
			<jsp:include page="commons/head.jsp"></jsp:include>
		</div>
	</div>
	<div class="container-fluid"
		style="padding-top: 70px; padding-bottom: 20px;">
		<div class="container containerbox">
			<div class="row">
				<div class="col-sm-6">
					<div style="float: left; padding-right: 10px;">
						<img class="img-rounded"
							style="text-align: center; max-width: 34px;"
							src="${empty minlogourl?'webfile/Publogo.do':minlogourl }">
					</div><jsp:include page="commons/searchBox.jsp"></jsp:include></div>
				<div class="col-sm-3"></div>
			</div>
			<div class="row" style="margin-top: 50px;">
				<div class="col-sm-8">
					<div class="wsch-searchinfo"
						style="border-bottom: 1px dashed #cccccc; margin-bottom: 20px;">
						查询到相关结果${result.totalSize}个&nbsp;&nbsp; <span
							style="color: #cccccc; font-size: 12px; white-space: nowrap;">结果id:${result.resultId}</span>
						<jsp:include page="commons/sortBox.jsp"></jsp:include>
					</div>
					<div></div>
					<c:forEach items="${result.list}" var="node">
						<%@ include file="commons/resultUnitExport.jsp"%>
					</c:forEach>
					<!-- 分页 -->
					<nav aria-label="Page navigation">
						<ul class="pagination">
							<c:if test="${result.currentPage>1 }">
								<li><a href="javascript:doSearch(${result.currentPage-1 })"
									aria-label="Previous"> <i
										class="glyphicon glyphicon-chevron-left"></i>
								</a></li>
							</c:if>
							<c:forEach begin="${result.exampleStart}"
								end="${result.exampleEnd}" var="cnum">
								<c:if test="${cnum!=result.currentPage }">
									<li><a href="javascript:doSearch(${cnum})">${cnum}</a></li>
								</c:if>
								<c:if test="${cnum==result.currentPage }">
									<li class="active"><a>${result.currentPage}</a></li>
								</c:if>
							</c:forEach>
							<c:if test="${result.currentPage<result.totalPage}">
								<li><a href="javascript:doSearch(${result.currentPage+1 })"
									aria-label="Next"> <i
										class="glyphicon glyphicon-chevron-right"></i>
								</a></li>
							</c:if>
						</ul>
					</nav>
				</div>
				<div class="hidden-xs col-sm-4">
					<jsp:include page="commons/dicList.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding: 0px;">
		<!-- 页脚 -->
		<jsp:include page="../commons/foot.jsp"></jsp:include>
	</div>
</body>
</html>