package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：业务字典类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "AppDic")
@Table(name = "wsch_p_appdic")
public class Appdic implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "APPID", length = 32, nullable = false)
        private String appid;
        @Column(name = "DICID", length = 32, nullable = false)
        private String dicid;

        public String  getAppid() {
          return this.appid;
        }
        public void setAppid(String appid) {
          this.appid = appid;
        }
        public String  getDicid() {
          return this.dicid;
        }
        public void setDicid(String dicid) {
          this.dicid = dicid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}