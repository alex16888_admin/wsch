package com.wuc.client.domain;

import org.apache.commons.lang.StringUtils;

public class Values {
	private String text1;
	private String text2;
	private String text3;
	private String text4;
	private String text5;

	private String textTitle1;
	private String textTitle2;
	private String textTitle3;
	private String textTitle4;
	private String textTitle5;

	private Float num1;
	private Float num2;
	private Float num3;
	private Float num4;
	private Float num5;

	private String numTitle1;
	private String numTitle2;
	private String numTitle3;
	private String numTitle4;
	private String numTitle5;

	public void setText1(String text, String title) {
		if (StringUtils.isNotBlank(text)) {
			this.text1 = text;
			this.textTitle1 = title;
		}
	}

	public void setText2(String text, String title) {
		if (StringUtils.isNotBlank(text)) {
			this.text2 = text;
			this.textTitle2 = title;
		}
	}

	public void setText3(String text, String title) {
		if (StringUtils.isNotBlank(text)) {
			this.text3 = text;
			this.textTitle3 = title;
		}
	}

	public void setText4(String text, String title) {
		if (StringUtils.isNotBlank(text)) {
			this.text4 = text;
			this.textTitle4 = title;
		}
	}

	public void setText5(String text, String title) {
		if (StringUtils.isNotBlank(text)) {
			this.text5 = text;
			this.textTitle5 = title;
		}
	}

	public void setNum1(Float num, String title) {
		if (num != null) {
			this.num1 = num;
			this.numTitle1 = title;
		}
	}

	public void setNum2(Float num, String title) {
		if (num != null) {
			this.num2 = num;
			this.numTitle2 = title;
		}
	}

	public void setNum3(Float num, String title) {
		if (num != null) {
			this.num3 = num;
			this.numTitle3 = title;
		}
	}

	public void setNum4(Float num, String title) {
		if (num != null) {
			this.num4 = num;
			this.numTitle4 = title;
		}
	}

	public void setNum5(Float num, String title) {
		if (num != null) {
			this.num5 = num;
			this.numTitle5 = title;
		}
	}

	public String getText1() {
		return text1;
	}

	public String getText2() {
		return text2;
	}

	public String getText3() {
		return text3;
	}

	public String getText4() {
		return text4;
	}

	public String getText5() {
		return text5;
	}

	public String getTextTitle1() {
		return textTitle1;
	}

	public String getTextTitle2() {
		return textTitle2;
	}

	public String getTextTitle3() {
		return textTitle3;
	}

	public String getTextTitle4() {
		return textTitle4;
	}

	public String getTextTitle5() {
		return textTitle5;
	}

	public Object getNum1() {
		return num1;
	}

	public Object getNum2() {
		return num2;
	}

	public Object getNum3() {
		return num3;
	}

	public Object getNum4() {
		return num4;
	}

	public Object getNum5() {
		return num5;
	}

	public String getNumTitle1() {
		return numTitle1;
	}

	public String getNumTitle2() {
		return numTitle2;
	}

	public String getNumTitle3() {
		return numTitle3;
	}

	public String getNumTitle4() {
		return numTitle4;
	}

	public String getNumTitle5() {
		return numTitle5;
	}

	public void setInt1(int num, String title) {
		if (num != 0) {
			this.num1 = Float.valueOf(num);
			this.numTitle1 = title;
		}
	}

	public void setInt2(int num, String title) {
		if (num != 0) {
			this.num2 = Float.valueOf(num);
			this.numTitle2 = title;
		}
	}

	public void setInt3(int num, String title) {
		if (num != 0) {
			this.num3 = Float.valueOf(num);
			this.numTitle3 = title;
		}
	}

	/**
	 * 通过text12345来保存大文本
	 * 
	 * @param text
	 * @param string
	 */
	public void setText12345(String text) {
		int sitNum = text.length();
		if (sitNum > 512 * 1) {
			this.text1 = text.substring(512 * 0, 512 * 1);
		} else {
			this.text1 = text;
			return;
		}
		if (sitNum > 512 * 2) {
			this.text2 = text.substring(512 * 1, 512 * 2);
		} else {
			this.text2 = text.substring(512 * 1);
			return;
		}
		if (sitNum > 512 * 3) {
			this.text3 = text.substring(512 * 2, 512 * 3);
		} else {
			this.text3 = text.substring(512 * 2);
			return;
		}
		if (sitNum > 512 * 4) {
			this.text4 = text.substring(512 * 3, 512 * 4);
		} else {
			this.text4 = text.substring(512 * 3);
			return;
		}
		if (sitNum > 512 * 5) {
			this.text5 = text.substring(512 * 4, 512 * 5);
		} else {
			this.text5 = text.substring(512 * 4);
			return;
		}
	}

	public static void main(String[] args) {
		for (int m = 0; m < 134; m++) {
			String text = "";
			for (int n = 0; n < m; n++) {
				text = text + "1";
			}
			System.out.println("text length:" + text.length() + ",m:" + m);
			Values val = new Values();
			val.setTextTitle12345(text);
			if (val.getTextTitle1() != null) {
				System.out.print("texttitle1:" + val.getTextTitle1().length());
			}
			if (val.getTextTitle2() != null) {
				System.out.print("texttitle2:" + val.getTextTitle2().length());
			}
			if (val.getTextTitle3() != null) {
				System.out.print("texttitle3:" + val.getTextTitle3().length());
			}
			if (val.getTextTitle4() != null) {
				System.out.print("texttitle4:" + val.getTextTitle4().length());
			}
			if (val.getTextTitle5() != null) {
				System.out.print("texttitle5:" + val.getTextTitle5().length());
			}
			System.out.println("------------------------------");
		}
	}

	/**
	 * 通过texttitle12345来保存大文本
	 * 
	 * @param linkHtml
	 * @param string
	 */
	public void setTextTitle12345(String text) {
		int sitNum = text.length();
		if (sitNum > 128 * 1) {
			this.textTitle1 = text.substring(128 * 0, 128 * 1);
		} else {
			this.textTitle1 = text;
			return;
		}
		if (sitNum > 128 * 2) {
			this.textTitle2 = text.substring(128 * 1, 128 * 2);
		} else {
			this.textTitle2 = text.substring(128 * 1);
			return;
		}
		if (sitNum > 128 * 3) {
			this.textTitle3 = text.substring(128 * 2, 128 * 3);
		} else {
			this.textTitle3 = text.substring(128 * 2);
			return;
		}
		if (sitNum > 128 * 4) {
			this.textTitle4 = text.substring(128 * 3, 128 * 4);
		} else {
			this.textTitle4 = text.substring(128 * 3);
			return;
		}
		if (sitNum > 128 * 5) {
			this.textTitle5 = text.substring(128 * 4, 128 * 5);
		} else {
			this.textTitle5 = text.substring(128 * 4);
			return;
		}
	}

}
