package com.farm.iproject.service.impl;

import com.farm.iproject.domain.PopGroupkey;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.PopgroupkeyDaoInter;
import com.farm.iproject.service.PopgroupkeyServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：组权限服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class PopgroupkeyServiceImpl implements PopgroupkeyServiceInter {
	@Resource
	private PopgroupkeyDaoInter popgroupkeyDaoImpl;

	private static final Logger log = Logger.getLogger(PopgroupkeyServiceImpl.class);

	@Override
	@Transactional
	public PopGroupkey insertPopgroupkeyEntity(PopGroupkey entity, LoginUser user) {
		// entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		entity.setPstate("1");
		return popgroupkeyDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public PopGroupkey editPopgroupkeyEntity(PopGroupkey entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		PopGroupkey entity2 = popgroupkeyDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setGroupid(entity.getGroupid());
		entity2.setPopkeyid(entity.getPopkeyid());
		entity2.setPstate(entity.getPstate());
		entity2.setCtime(entity.getCtime());
		entity2.setPcontent(entity.getPcontent());
		entity2.setId(entity.getId());
		popgroupkeyDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deletePopgroupkeyEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		popgroupkeyDaoImpl.deleteEntity(popgroupkeyDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public PopGroupkey getPopgroupkeyEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return popgroupkeyDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createPopgroupkeySimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "WSCH_A_POPGOURPKEY a left join WSCH_A_POPGOURP b on a.groupid=b.id",
				"a.ID as ID,a.GROUPID as GROUPID,a.POPKEYID as POPKEYID,a.PSTATE as PSTATE,a.CTIME as CTIME,a.PCONTENT as PCONTENT,b.groupname as GROUPNAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public PopGroupkey addGroupKey(String groupid, String popkeyid) {
		if (StringUtils.isNotBlank(groupid) && StringUtils.isNotBlank(popkeyid)) {
			List<PopGroupkey> list=		popgroupkeyDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("GROUPID", groupid, "="))
					.add(new DBRule("POPKEYID", popkeyid, "=")).toList());
			if(list.size()<=0) {
				PopGroupkey entity = new PopGroupkey();
				entity.setCtime(TimeTool.getTimeDate14());
				entity.setGroupid(groupid);
				entity.setPopkeyid(popkeyid);
				entity.setPstate("1");
				return popgroupkeyDaoImpl.insertEntity(entity);
			}else {
				return list.get(0);
			}
			
		}
		return null;
	}

	@Override
	@Transactional
	public void addGroupKey(String groupid, List<String> parseIds) {
		for (String id : parseIds) {
			addGroupKey(groupid, id);
		}
	}

}
