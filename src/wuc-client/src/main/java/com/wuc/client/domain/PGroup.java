package com.wuc.client.domain;

import java.util.List;

public class PGroup {
	private String id;
	private String name;
	private String groupkey;
	private String state;
	private List<Para> paras;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupkey() {
		return groupkey;
	}

	public void setGroupkey(String groupkey) {
		this.groupkey = groupkey;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<Para> getParas() {
		return paras;
	}

	public void setParas(List<Para> paras) {
		this.paras = paras;
	}

}
