<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<style>
.footServerUl {
	float: left;
}

.footServerUl img {
	float: left;
	width: 24px;
	height: 24px;
	background-color: #ffffff;
}

.footServerUl a {
	color: #b0b6bb;
}

.putServerBox ul li {
	list-style-type: none;
	float: left;
	margin: 2px;
	margin-left: 10px;
	color: #888;
	font-size: 14px;
	height: 2em;
	line-height: 20px;
	padding-left: 29px;
	text-decoration: none;
	text-decoration-color: #888;
	text-decoration-line: none;
	text-decoration-style: solid;
	white-space: nowrap;
	text-align: left;
}

.putServerBox ul li a {
	color: #888;
}

.putServerBox ul li a:hover {
	color: #ba2636;
}
</style>

<script type="text/javascript">
	$(function() {
		$(window).resize(function() {
			$('.containerbox').css('min-height', $(window).height() - 220);
		});
		$('.containerbox').css('min-height', $(window).height() - 220);
	});
</script>
<div class="container-fluid" style="background-color: #f8fafc;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="putServerBox" style="padding-top: 20px;">
					<ul id="recommendServiceList" class="footServerUl">
						<PF:WebUrls var="node" type="1">
							<li><a href='${node.LINKURL}'> <c:if
										test="${!empty node.IMGURL}">
										<img class="imgMenuIcon img-circle"
											style="width: 24px; height: 24px; position: relative; top: -2px;"
											alt="${node.NAME}" src="${node.IMGURL}" />&nbsp;</c:if>
									${node.NAME}
							</a></li>
						</PF:WebUrls>
					</ul>
				</div>
				<div
					style="height: 100px; padding: 50px; text-align: center; color: #99a1a6;">
					<PF:ParameterValue key="config.sys.foot" />
					- v
					<PF:ParameterValue key="config.sys.version" />
					<c:if test="${USEROBJ!=null&&!empty USERMENU}">
						-<a style="color: #99a1a6;" href="<PF:basePath/>home/Pubinfo.html">授权</a>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>