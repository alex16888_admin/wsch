package com.farm.wsch.index.pojo;

/**
 * 字典统计项封装
 * 
 * @author macpl
 *
 */
public class ResultDicNum {
	// 无意义，存储字段
	private String name;
	// 无意义，存储字段
	private String id;
	// 无意义，存储字段
	private String parentid;
	// 无意义，存储字段
	private String domain;
	// 要统计测字段域key
	private String fieldkey;
	// 要匹配的值
	private String valkey;
	// 要匹配的策略（全文匹配，或子串匹配）
	private PeekType peektype;
	// 匹配次数
	private long num = 0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getValkey() {
		return valkey;
	}

	public void setValkey(String valkey) {
		this.valkey = valkey;
	}

	public String getFieldkey() {
		return fieldkey;
	}

	public PeekType getPeektype() {
		return peektype;
	}

	public void setPeektype(PeekType peektype) {
		this.peektype = peektype;
	}

	public void setFieldkey(String fieldkey) {
		this.fieldkey = fieldkey;
	}

	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public enum PeekType {
		/**
		 * 字典为字段子串
		 */
		indexOfS, /**
					 * 字典和字段完全匹配
					 */
		equals
	}
}
