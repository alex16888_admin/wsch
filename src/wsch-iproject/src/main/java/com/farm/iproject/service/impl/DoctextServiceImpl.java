package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Doctext;
import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Pdocument;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.DoctextDaoInter;
import com.farm.iproject.service.DoctextServiceInter;
import com.farm.iproject.utils.JsonUtils;
import com.farm.util.web.HtmlUtils;
import com.farm.wsch.index.WschIndexServerInter;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：大文本服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class DoctextServiceImpl implements DoctextServiceInter {
	@Resource
	private DoctextDaoInter doctextDaoImpl;
	// 默认每个字段最大长度（单位字符）
	private static int MAX_FIELD_LENGHT = 10000;

	private static final Logger log = Logger.getLogger(DoctextServiceImpl.class);

	@Override
	@Transactional
	public Doctext insertText(Map<String, String> oparas, Map<String, Field> fieldDic, String projectid,
			LoginUser currentUser) {
		int maxlenght = MAX_FIELD_LENGHT;
		Map<String, String> paras = new HashMap<String, String>();
		for (Map.Entry<String, String> node : oparas.entrySet()) {
			Field field = fieldDic.get(node.getKey());
			if (field != null && field.getMaxlenght() != null && field.getMaxlenght() != 0) {
				maxlenght = field.getMaxlenght();
			}
			String otext = node.getValue();
			String text = HtmlUtils.HtmlRemoveTag(node.getValue());
			if (text.length() > maxlenght) {
				text = text.substring(0, maxlenght);
			}
			if (otext.length() > maxlenght) {
				otext = otext.substring(0, maxlenght);
			}
			node.setValue(otext);
			paras.put(node.getKey(), text);
		}
		String text = JsonUtils.toJsonStr(paras);
		String otext = JsonUtils.toJsonStr(oparas);
		Doctext entity = new Doctext();
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(currentUser.getName());
		entity.setOtext(otext);
		entity.setProjectid(projectid);
		entity.setPstate("1");
		entity.setText(text);
		entity = doctextDaoImpl.insertEntity(entity);
		return entity;
	}

	@Override
	@Transactional
	public Doctext insertDoctextEntity(Doctext entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return doctextDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Doctext editDoctextEntity(Doctext entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Doctext entity2 = doctextDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setText(entity.getText());
		entity2.setPcontent(entity.getPcontent());
		entity2.setCtime(entity.getCtime());
		entity2.setPstate(entity.getPstate());
		entity2.setCusername(entity.getCusername());
		entity2.setId(entity.getId());
		doctextDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteDoctextEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		doctextDaoImpl.deleteEntity(doctextDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Doctext getDoctextEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return doctextDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createDoctextSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WSCH_P_DOCTEXT", "ID,TEXT,PCONTENT,CTIME,PSTATE,CUSERNAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public Map<String, Object> getDocMap(Pdocument document, boolean isOtext_Html) {
		Doctext doctext = doctextDaoImpl.getEntity(document.getTextid());
		String text = null;
		if (isOtext_Html) {
			text = doctext.getOtext();
		} else {
			text = doctext.getText();
		}
		Map<String, Object> map = (Map<String, Object>) JSON.parse(text);
		map.put(WschIndexServerInter.DefaultField.APPID.getKey(), document.getAppid());
		map.put(WschIndexServerInter.DefaultField.APPKEYID.getKey(), document.getAppkeyid());
		map.put(WschIndexServerInter.DefaultField.POPKEYS.getKey(), document.getPopkeys());
		return map;
	}

}
