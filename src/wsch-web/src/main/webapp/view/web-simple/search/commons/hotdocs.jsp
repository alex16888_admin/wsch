<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<style>
.hotDoc_box {
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	line-height: 2em;
}

.hotDoc_box a {
	color: #333333;
	text-decoration: none;
}

.hotDoc_box a:HOVER {
	color: #315efb;
	text-decoration: none;
}
</style>
<c:forEach items="${hotdocs }" var="node" varStatus="inde">
	<div class="hotDoc_box" title="${node.TITLE }/${node.NUM }">
		<span style="width: 50px;"> ${inde.index+1} <c:if
				test="${inde.index+1<10}"> 
							&nbsp;
							</c:if>
		</span><a
			href="document/Publink.do?appid=${node.APPID}&projectid=${projectid}"
			target="_blank">${node.TITLE }</a>
	</div>
</c:forEach>