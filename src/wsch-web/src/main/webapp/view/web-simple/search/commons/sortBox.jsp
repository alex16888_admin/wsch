<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<c:if test="${!empty sorts }">
	<div class="btn-group" style="float: right;">
		<button type="button" class="btn btn-default  btn-xs dropdown-toggle"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			&nbsp;&nbsp;排序:<span id="sortTitleId"></span><span id="sortTypeId"></span>&nbsp;&nbsp;
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="javascript:setSortTypeId(null,null,null,true)">相关度</a></li>
			<li role="separator" class="divider"></li>
			<c:forEach items="${sorts}" var="node">
				<li><a
					href="javascript:setSortTypeId('${node.keyid}','${node.name}','ASC',true)">${node.name}&nbsp;<i
						class="glyphicon glyphicon-arrow-up"></i></a></li>
				<li><a
					href="javascript:setSortTypeId('${node.keyid}','${node.name}','DESC',true)">${node.name}&nbsp;<i
						class="glyphicon glyphicon-arrow-down"></i></a></li>
			</c:forEach>
		</ul>
	</div>
	<script type="text/javascript">
		$(function() {
			setSortTypeId('${sortkey}', '${sorttitle}', '${sorttype}', false);
		})
		//ASC/DESC
		function setSortTypeId(field, title, type, dosearchIs) {
			if (!field) {
				$('#sortTitleId').text('相关度');
				$('#sortTypeId').html('');
				$('#SORTKEY_ID').val('');
				$('#SORTTYPE_ID').val('');
				$('#SORTTITLE_ID').val('');
			} else {
				$('#SORTKEY_ID').val(field);
				$('#SORTTYPE_ID').val(type);
				$('#SORTTITLE_ID').val(title);
				$('#sortTitleId').text(title);
				$('#sortTypeId')
						.html(
								type == 'ASC' ? '&nbsp;<i class="glyphicon glyphicon-arrow-up"></i>'
										: '&nbsp;<i class="glyphicon glyphicon-arrow-down"></i>');
			}
			if (dosearchIs) {
				doSearch(1);
			}
		}
	</script>
</c:if>