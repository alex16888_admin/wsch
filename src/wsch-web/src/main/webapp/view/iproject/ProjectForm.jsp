<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--检索项目表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formProject">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">项目名称:</td>
					<td colspan="3"><input type="text" style="width: 260px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td colspan="1" rowspan="4"
						style="border-left: 1px solid #cccccc; text-align: center;"><img
						style="width: 64px; height: 64px;" alt="" id="subsysImgId"
						class="img-thumbnail"
						src="${imgurl==null?'webfile/Publogo.do':imgurl}"> <c:if
							test="${pageset.operateType!=0}">
							<br />
							<span style="font-size: 12px;">宽48*高48</span>
							<br />
							<input type="hidden" name="minimgid" id="imgidId"
								value="${entity.minimgid}">
							<jsp:include
								page="/view/web-simple/commons/fileUploadCommons.jsp">
								<jsp:param value="subsysimg" name="appkey" />
								<jsp:param value="IMG" name="type" />
								<jsp:param
									value="上传小logo<span style='color:red;'>&nbsp;*</span>"
									name="title" />
							</jsp:include>
							<a href="javascript:delImg('imgidId','subsysImgId')">删除</a>
						</c:if></td>
					<td colspan="1" rowspan="4"
						style="border-left: 1px solid #cccccc; text-align: center;"><img
						style="width: 256px; height: 64px;" alt="" id="logomaxImgId"
						class="img-thumbnail"
						src="${imgMaxurl==null?'webfile/PubHomelogo.do':imgMaxurl}">
						<c:if test="${pageset.operateType!=0}">
							<br />
							<span style="font-size: 12px;">宽256*高64</span>
							<br />
							<input type="hidden" name="maximgid" id="imgMaxidId"
								value="${entity.maximgid}">
							<jsp:include
								page="/view/web-simple/commons/fileUploadCommons.jsp">
								<jsp:param value="imgMaxidId" name="appkey" />
								<jsp:param value="IMG" name="type" />
								<jsp:param value="上传MAX图" name="title" />
							</jsp:include>
							<a href="javascript:delImg('imgMaxidId','logomaxImgId')">删除</a>
						</c:if></td>
				</tr>
				<tr>
					<td class="title">KEYID:</td>
					<td colspan="3"><input type="text" style="width: 260px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_keyid" name="keyid" value="${entity.keyid}"></td>
				</tr>
				<tr>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 64px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
					<td class="title">状态:</td>
					<td><select class="easyui-validatebox" id="entity_pstate"
						name="pstate" data-options="required:true" val="${entity.pstate}">
							<option value="1">待发布</option>
							<option value="2">发布</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea name="pcontent"
							style="width: 260px;" class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							rows="" cols="">${entity.pcontent}</textarea></td>
				</tr>

				<!-- 
				<tr>
					<td class="title">状态:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_pstate" name="pstate" value="${entity.pstate}">
					</td>
				</tr> -->
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityProject" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityProject" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formProject" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionProject = 'project/add.do';
	var submitEditActionProject = 'project/edit.do';
	var currentPageTypeProject = '${pageset.operateType}';
	var submitFormProject;
	$(function() {
		//表单组件对象
		submitFormProject = $('#dom_formProject').SubmitForm({
			pageType : currentPageTypeProject,
			grid : gridProject,
			currentWindowId : 'winProject'
		});
		//关闭窗口
		$('#dom_cancle_formProject').bind('click', function() {
			$('#winProject').window('close');
		});
		//提交新增数据
		$('#dom_add_entityProject').bind('click', function() {
			submitFormProject.postSubmit(submitAddActionProject);
		});
		//提交修改数据
		$('#dom_edit_entityProject').bind('click', function() {
			submitFormProject.postSubmit(submitEditActionProject);
		});
	});
	//图片提交成功
	function fileUploadHandle(appkey, url, id, fileName) {
		if ('imgMaxidId' == appkey) {
			$('#logomaxImgId').attr('src', url);
			$('#imgMaxidId').val(id);
		}
		if ('subsysimg' == appkey) {
			$('#subsysImgId').attr('src', url);
			$('#imgidId').val(id);
		}
	}
	//删除表单图片
	function delImg(inputid,imgid){
		$('#'+inputid).val('');
		$('#'+imgid).attr('src','webfile/Publogo.do');
	}
//-->
</script>