<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--业务字典表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formAppdic">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<input type="hidden" id="entity_APPID" name="appid" value="${appmodel.id}">
			<table class="editTable">
				<tr>
					<td class="title">DICID:</td>
					<td colspan="3"><select class="easyui-validatebox"
						data-options="required:true" id="entity_dicid" name="dicid"
						val="${entity.dicid}">
							<option value="">~请选择~</option>
							<c:forEach items="${dics}" var="node">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityAppdic" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityAppdic" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formAppdic" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionAppdic = 'appdic/add.do';
	var submitEditActionAppdic = 'appdic/edit.do';
	var currentPageTypeAppdic = '${pageset.operateType}';
	var submitFormAppdic;
	$(function() {
		//表单组件对象
		submitFormAppdic = $('#dom_formAppdic').SubmitForm({
			pageType : currentPageTypeAppdic,
			grid : gridAppdic,
			currentWindowId : 'winAppdic'
		});
		//关闭窗口
		$('#dom_cancle_formAppdic').bind('click', function() {
			$('#winAppdic').window('close');
		});
		//提交新增数据
		$('#dom_add_entityAppdic').bind('click', function() {
			submitFormAppdic.postSubmit(submitAddActionAppdic);
		});
		//提交修改数据
		$('#dom_edit_entityAppdic').bind('click', function() {
			submitFormAppdic.postSubmit(submitEditActionAppdic);
		});
	});
//-->
</script>