package com.farm.wuc.events;

/**
 * 事件模型
 * 
 * @author macpl
 *
 */
public enum WucEventModle {
	// 检索
	WSCH_SEARCH("资源检索"), WSCH_VISIT("资源访问");

	private String key;
	private String title;
	private String note;

	private WucEventModle(String title) {
		this.key = this.name();
		this.title = title;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
