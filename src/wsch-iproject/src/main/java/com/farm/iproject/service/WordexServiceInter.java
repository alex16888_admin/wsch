package com.farm.iproject.service;

import com.farm.iproject.domain.Wordex;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：扩展字典服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface WordexServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Wordex insertWordexEntity(Wordex entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Wordex editWordexEntity(Wordex entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteWordexEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Wordex getWordexEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createWordexSimpleQuery(DataQuery query);

	/**
	 * 将词典写入文件（单条）
	 * 
	 * @param id
	 */
	public void addWordToFile(String id);

	/**
	 * 将词典写入文件（全部）
	 * 
	 * @param id
	 */
	public void reLoadToFile();
}