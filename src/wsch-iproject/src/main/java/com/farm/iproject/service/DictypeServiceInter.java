package com.farm.iproject.service;

import com.farm.iproject.domain.Appdic;
import com.farm.iproject.domain.Dictype;
import com.farm.wsch.index.pojo.ResultDicNum;
import com.farm.wsch.index.pojo.SearchRule;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：字典类型服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface DictypeServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Dictype insertDictypeEntity(Dictype entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Dictype editDictypeEntity(Dictype entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteDictypeEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Dictype getDictypeEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createDictypeSimpleQuery(DataQuery query);

	/**
	 * 获得项目字典
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Dictype> getDictypes(String projectid);

	/**
	 * 获得一级字典
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Dictype> getRootDictype(String projectid);

	/**
	 * 用于结果统计的标记统计对象
	 * 
	 * @param projectid
	 * @return
	 */
	public List<ResultDicNum> getResultDicNums(String projectid, String appkey);

	/**
	 * 获得一个检索条件
	 * 
	 * @param dicrule
	 * @return
	 */
	public SearchRule getSearchRule(String dictypeid);

	/**
	 * 获得一级字典
	 * 
	 * @param projectid
	 * @param keyid
	 * @return
	 */
	public Dictype getDictype(String projectid, String keyid);

	/**
	 * 清理所有子项（保留一级）
	 * 
	 * @param dictypeId 一级字典id
	 */
	public void deleteAllSubDictype(String dictypeId);
}