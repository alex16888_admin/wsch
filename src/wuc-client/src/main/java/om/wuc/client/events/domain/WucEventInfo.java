package om.wuc.client.events.domain;

import org.apache.commons.lang.StringUtils;

import com.wuc.client.domain.Values;

/**
 * 事件参数
 * 
 * @author macpl
 *
 */
public class WucEventInfo {
	private String objid;// 事件主体对象ID 必填，如关注知识事件的知识id
	private String objname;// 事件主体对象名称 必填，如关注知识事件的知识名称
	private Float num1;// 数值参数 可空,float类型
	private String numtitle1;// 数值参数标题 可空
	private Float num2;// 数值参数 可空,float类型
	private String numtitle2; // 数值参数标题 可空
	private Float num3;// 数值参数 可空,float类型
	private String numtitle3;// 数值参数标题 可空
	private Float num4;// 数值参数 可空,float类型
	private String numtitle4;// 数值参数标题 可空
	private Float num5;// 数值参数 可空,float类型
	private String numtitle5;// 数值参数标题 可空
	private String text1;// 文本参数 可空,字符串类型
	private String texttitle1; // 文本参数标题 可空
	private String text2; // 文本参数 可空,字符串类型
	private String texttitle2; // 文本参数标题 可空
	private String text3; // 文本参数 可空,字符串类型
	private String texttitle3; // 文本参数标题 可空
	private String text4;// 文本参数 可空,字符串类型
	private String texttitle4; // 文本参数标题 可空
	private String text5; // 文本参数 可空,字符串类型
	private String texttitle5;// 文本参数标题 可空

	@SuppressWarnings("unused")
	private WucEventInfo() {
	}

	public WucEventInfo(String objid, String objname) {
		super();
		this.objid = objid;
		this.objname = objname;
	}

	public WucEventInfo addText1(String text, String title) {
		text1 = text;
		texttitle1 = title;
		return this;
	}

	public WucEventInfo addText2(String text, String title) {
		text2 = text;
		texttitle2 = title;
		return this;
	}

	public WucEventInfo addText3(String text, String title) {
		text3 = text;
		texttitle3 = title;
		return this;
	}

	public WucEventInfo addText4(String text, String title) {
		text4 = text;
		texttitle4 = title;
		return this;
	}

	public WucEventInfo addText5(String text, String title) {
		text5 = text;
		texttitle5 = title;
		return this;
	}

	public WucEventInfo addNum1(float num, String title) {
		num1 = num;
		numtitle1 = title;
		return this;
	}

	public WucEventInfo addNum2(float num, String title) {
		num2 = num;
		numtitle2 = title;
		return this;
	}

	public WucEventInfo addNum3(float num, String title) {
		num3 = num;
		numtitle3 = title;
		return this;
	}

	public WucEventInfo addNum4(float num, String title) {
		num4 = num;
		numtitle4 = title;
		return this;
	}

	public WucEventInfo addNum5(float num, String title) {
		num5 = num;
		numtitle5 = title;
		return this;
	}

	public String getObjid() {
		return objid;
	}

	public String getObjname() {
		return objname;
	}

	public Float getNum1() {
		return num1;
	}

	public String getNumtitle1() {
		return numtitle1;
	}

	public Float getNum2() {
		return num2;
	}

	public String getNumtitle2() {
		return numtitle2;
	}

	public Float getNum3() {
		return num3;
	}

	public String getNumtitle3() {
		return numtitle3;
	}

	public Float getNum4() {
		return num4;
	}

	public String getNumtitle4() {
		return numtitle4;
	}

	public Float getNum5() {
		return num5;
	}

	public String getNumtitle5() {
		return numtitle5;
	}

	public String getText1() {
		return text1;
	}

	public String getTexttitle1() {
		return texttitle1;
	}

	public String getText2() {
		return text2;
	}

	public String getTexttitle2() {
		return texttitle2;
	}

	public String getText3() {
		return text3;
	}

	public String getTexttitle3() {
		return texttitle3;
	}

	public String getText4() {
		return text4;
	}

	public String getTexttitle4() {
		return texttitle4;
	}

	public String getText5() {
		return text5;
	}

	public String getTexttitle5() {
		return texttitle5;
	}

	public Values getValues() {
		Values values = new Values();
		if(getNum1()!=null)
		values.setNum1(getNum1(), getNumtitle1());
		if(getNum2()!=null)
		values.setNum2(getNum2(), getNumtitle2());
		if(getNum3()!=null)
		values.setNum3(getNum3(), getNumtitle3());
		if(getNum4()!=null)
		values.setNum4(getNum4(), getNumtitle4());
		if(getNum5()!=null)
		values.setNum5(getNum5(), getNumtitle5());
		if(StringUtils.isNotBlank(getText1()))
		values.setText1(getText1(), getTexttitle1());
		if(StringUtils.isNotBlank(getText2()))
		values.setText2(getText2(), getTexttitle2());
		if(StringUtils.isNotBlank(getText3()))
		values.setText3(getText3(), getTexttitle3());
		if(StringUtils.isNotBlank(getText4()))
		values.setText4(getText4(), getTexttitle4());
		if(StringUtils.isNotBlank(getText5()))
		values.setText5(getText5(), getTexttitle5());
		return values;
	}

}
