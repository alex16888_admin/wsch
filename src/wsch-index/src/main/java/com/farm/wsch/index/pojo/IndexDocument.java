package com.farm.wsch.index.pojo;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.FloatPoint;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

import com.farm.wsch.index.WschIndexServerInter.DefaultField;
import com.farm.wsch.index.WschIndexServerInter.FieldType;

public class IndexDocument {
	private Document doc = new Document();
	private String id;

	public Document getDocument() {
		return doc;
	}

	public void addField(FieldType fieldType, String text, String keyid, boolean isStore) {
		if (text == null) {
			text = "";
		}
		if (fieldType.equals(FieldType.StringField)) {
			doc.add(new StringField(keyid, text, isStore ? Field.Store.YES : Field.Store.NO));
		}
		if (fieldType.equals(FieldType.TextField)) {
			doc.add(new TextField(keyid, text, isStore ? Field.Store.YES : Field.Store.NO));
		}
		if (fieldType.equals(FieldType.FloatField)) {
			doc.add(new FloatPoint(keyid, Float.valueOf(text)));
		}
		if (keyid.equals(DefaultField.APPID.getKey())) {
			id = text;
		}
	}

	public String getId() {
		return id;
	}

}
