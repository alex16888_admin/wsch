package com.farm.wsch.index.pojo;

import java.util.Date;

public class RuntimeInfo {
	private String title;
	private long start = new Date().getTime();
	private float runtime;
	private int resultNum;

	@SuppressWarnings("unused")
	private RuntimeInfo() {
	}

	public RuntimeInfo(String title) {
		this.title = title;
	}

	public void runEnd(int num) {
		runtime = (new Date().getTime() - start) / 1000.00f;
		if (runtime == 0) {
			runtime = 0.001f;
		}
		this.resultNum = num;
	}

	public String getTitle() {
		return title;
	}

	public float getRuntime() {
		return runtime;
	}

	public int getResultNum() {
		return resultNum;
	}

}
