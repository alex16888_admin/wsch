package com.farm.wcp.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.service.UserServiceInter;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.page.ViewMode;
import com.farm.sfile.WdapFileServiceInter;
import com.farm.wcp.util.CheckCodeUtil;
import com.farm.web.WebUtils;

/**
 * 用户空间操作
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/userspace")
@Controller
public class UserWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(UserWebController.class);
	@Resource
	private UserServiceInter userServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;

	/**
	 * 更新当前登录用户信息
	 * 
	 * @param id
	 *            用户ID
	 * @param name
	 *            用户名称
	 * @param photoid
	 *            头像ID
	 * @return ModelAndView
	 */
	@RequestMapping("/editCurrentUser")
	public ModelAndView editCurrentUser(String name, String photoid, HttpSession session) {
		try {
			// 修改用户信息
			LoginUser currentUser = getCurrentUser(session);
			String oldimgid = userServiceImpl.getUserEntity(currentUser.getId()).getImgid();
			wdapFileServiceImpl.freeFile(oldimgid);
			userServiceImpl.editCurrentUser(currentUser.getId(), name, photoid, null);
			wdapFileServiceImpl.submitFile(photoid);
			session.setAttribute("USEROBJ", userServiceImpl.getUserEntity(currentUser.getId()));
			return ViewMode.getInstance().putAttr("OK", "OK")
					.returnModelAndView("web-simple/userspace/user-setting-userinfo");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}

	}

	@RequestMapping("/editCurrentUserPwdCommit")
	public ModelAndView editCurrentUserPwdCommit(String password, String newPassword, String checkcode,
			HttpSession session, HttpServletRequest request) {
		try {
			// 是否启用登录验证码
			CheckCodeUtil.isCheckCodeAble(checkcode, session);
			// 如果验证失败会抛出异常
			userServiceImpl.editUserPassword(getCurrentUser(session).getId(), password, newPassword);
			return ViewMode.getInstance().putAttr("OK", "OK")
					.returnModelAndView("web-simple/userspace/user-setting-password");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("web-simple/userspace/user-setting-password");
		}

	}


	/***
	 * 信息修改
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/settinginfo")
	public ModelAndView settinginfo(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		try {
			return view.returnModelAndView("web-simple/userspace/user-setting-userinfo");
		} catch (Exception e) {
			return view.setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}
	}

	/***
	 * 密码修改
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/settingpsd")
	public ModelAndView settingpsd(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		try {
			return view.returnModelAndView("web-simple/userspace/user-setting-password");
		} catch (Exception e) {
			return view.setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}
	}
}
